/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50731
Source Host           : localhost:3306
Source Database       : tourismbase

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2021-03-28 10:41:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `oto_admin_nav`
-- ----------------------------
DROP TABLE IF EXISTS `oto_admin_nav`;
CREATE TABLE `oto_admin_nav` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单表',
  `pid` int(11) unsigned DEFAULT '0' COMMENT '所属菜单',
  `name` varchar(15) DEFAULT '' COMMENT '菜单名称',
  `mca` varchar(255) DEFAULT '' COMMENT '模块、控制器、方法',
  `ico` varchar(20) DEFAULT '' COMMENT 'font-awesome图标',
  `order_number` int(11) unsigned DEFAULT NULL COMMENT '排序',
  `platform` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '所属平台 1经销商,2运营商,3分销商',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oto_admin_nav
-- ----------------------------
INSERT INTO `oto_admin_nav` VALUES ('1', '0', '系统设置', 'Operator/ShowNav/config', 'cog', '8', '2');
INSERT INTO `oto_admin_nav` VALUES ('2', '1', '菜单管理', 'Operator/Nav/index', null, null, '2');
INSERT INTO `oto_admin_nav` VALUES ('7', '4', '权限管理', 'Operator/Rule/index', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('4', '0', '权限控制', 'Operator/ShowNav/rule', 'expeditedssl', '7', '2');
INSERT INTO `oto_admin_nav` VALUES ('8', '4', '用户组管理', 'Operator/Rule/group', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('9', '4', '管理员列表', 'Operator/Rule/admin_user_list', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('16', '0', '会员管理', 'Operator/ShowNav/Visitor', 'users', '10', '2');
INSERT INTO `oto_admin_nav` VALUES ('39', '0', '产品管理', 'Operator/Product/index', 'expeditedssl', '1', '2');
INSERT INTO `oto_admin_nav` VALUES ('40', '39', '自营线路发布', 'Operator/Line/add', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('42', '39', '线路列表', 'Operator/Product/lineList', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('43', '39', '团号管理', 'Operator/Product/groupManage', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('45', '0', '单品供应商', 'Operator/UnitShops', 'server', '5', '2');
INSERT INTO `oto_admin_nav` VALUES ('46', '45', '添加商家', 'Operator/Shops/addShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('47', '45', '交通供应商', 'Operator/Shops/trafficShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('48', '45', '酒店供应商', 'Operator/Shops/hotelShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('49', '45', '景区供应商', 'Operator/Shops/scenicShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('50', '45', '保险供应商', 'Operator/Shops/insureShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('58', '56', '签证列表', 'Operator/Visa/visaList', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('57', '56', '添加签证', 'Operator/Visa/addVisa', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('56', '0', '签证管理', 'Operator/Visa', 'pagelines', '6', '2');
INSERT INTO `oto_admin_nav` VALUES ('59', '39', '线路合作申请', 'Operator/Product/cooperationList', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('60', '0', '城市管理', 'Operator/City/country', 'map-marker', '9', '1');
INSERT INTO `oto_admin_nav` VALUES ('61', '60', '国家列表', 'Operator/City/country', '', null, '1');
INSERT INTO `oto_admin_nav` VALUES ('62', '60', '省列表', 'Operator/City/province', '', null, '1');
INSERT INTO `oto_admin_nav` VALUES ('64', '1', '修改密码', 'Operator/Platform/editPassword', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('67', '0', '财务管理', 'Operator/Finance', 'building', '3', '2');
INSERT INTO `oto_admin_nav` VALUES ('90', '1', '操作日志', 'Operator/Oplog/index', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('70', '67', '营业收入报表', 'Operator/Report/businessStatement', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('79', '0', '商家管理', 'Operator/Shops', 'server', '4', '2');
INSERT INTO `oto_admin_nav` VALUES ('72', '16', '会员列表', 'Operator/Visitor/visitor_list', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('73', '16', '积分设置', 'Operator/Score/score_config', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('74', '16', '等级设置', 'Operator/Visitor/grade_config', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('75', '67', '发票管理', 'Operator/Finance/bill_list', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('76', '67', '销售订单结算', 'Operator/Finance/settlement_list', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('77', '67', '分销商结算列表', 'Operator/Finance/reseller_list', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('78', '67', '员工销售业绩', 'Operator/Finance/performance_list', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('80', '79', '添加注册供应商', 'Operator/Shops/addLineShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('81', '79', '注册供应商列表', 'Operator/Shops/lineShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('82', '79', '添加分销商', 'Operator/Shops/addResellerShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('83', '79', '分销商列表', 'Operator/Shops/resellerShops', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('92', '0', '订单列表', 'orderList', 'file-text', '2', '2');
INSERT INTO `oto_admin_nav` VALUES ('93', '92', '订单列表', 'Operator/SaleManger/orderList', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('95', '79', '添加门店', 'Operator/Shops/addShop', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('87', '67', '门店结算列表', 'Operator/Report/selfIncome', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('89', '67', '财务结算', 'Operator/Report/financialManagement', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('91', '67', '供应商线路结算', 'Operator/Finance/financeList', '', null, '2');
INSERT INTO `oto_admin_nav` VALUES ('96', '79', '门店列表', 'Operator/Shops/shopList', '', null, '2');

-- ----------------------------
-- Table structure for `oto_areas`
-- ----------------------------
DROP TABLE IF EXISTS `oto_areas`;
CREATE TABLE `oto_areas` (
  `areaId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) NOT NULL,
  `areaName` varchar(20) DEFAULT NULL,
  `isShow` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1正常,其它为锁定',
  `areaSort` int(11) NOT NULL DEFAULT '0',
  `areaKey` char(255) NOT NULL,
  `areaType` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0省,1市,2区,4国家',
  `areaFlag` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`areaId`),
  KEY `isShow` (`isShow`,`areaFlag`),
  KEY `areaType` (`areaType`),
  KEY `parentId` (`parentId`)
) ENGINE=MyISAM AUTO_INCREMENT=820314 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oto_areas
-- ----------------------------

-- ----------------------------
-- Table structure for `oto_currency`
-- ----------------------------
DROP TABLE IF EXISTS `oto_currency`;
CREATE TABLE `oto_currency` (
  `currency_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(60) DEFAULT NULL COMMENT '货币名称',
  `currency_symbol` varchar(60) DEFAULT NULL COMMENT ' 货币符号',
  `add_time` int(11) unsigned DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='币种表';

-- ----------------------------
-- Records of oto_currency
-- ----------------------------
INSERT INTO `oto_currency` VALUES ('1', '人民币', '¥', '1507620623');

-- ----------------------------
-- Table structure for `oto_dealer_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `oto_dealer_auth_group`;
CREATE TABLE `oto_dealer_auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text COMMENT '规则id',
  `admin_id` int(11) unsigned NOT NULL COMMENT '所属哪一个管理员ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- ----------------------------
-- Records of oto_dealer_auth_group
-- ----------------------------
INSERT INTO `oto_dealer_auth_group` VALUES ('11', '系统管理员', '1', '167,168,169,173,174,175,176,185,186,187,188,170,189,190,191,192,193,194,195,196,171,197,198,199,200,201,202,203,204,172,177,178,179,180,181,182,183,184', '1');
INSERT INTO `oto_dealer_auth_group` VALUES ('12', '系统管理员', '1', '6,96,20,1,2,3,4,5,64,21,7,8,9,10,11,12,13,14,15,16,123,124,125,19,104,105,106,107,108,118,109,110,111,112,117,131,132', '4');
INSERT INTO `oto_dealer_auth_group` VALUES ('13', '文章编辑', '1', '6,96,20,1,2,3,4,5,64,142,143,21,7,8,9,10,11,12,13,14,15,16,123,19,104,105,106,107,108,118,109,110,111,112,117,131,132,134,135,136,137,138,140,141,144,145,151,154,155,156,157,158', '4');
INSERT INTO `oto_dealer_auth_group` VALUES ('15', '线路发布', '1', '131,132,211,212,213,214,215,216,219,222,217,218', '1');

-- ----------------------------
-- Table structure for `oto_dealer_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `oto_dealer_auth_group_access`;
CREATE TABLE `oto_dealer_auth_group_access` (
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `group_id` int(11) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

-- ----------------------------
-- Records of oto_dealer_auth_group_access
-- ----------------------------
INSERT INTO `oto_dealer_auth_group_access` VALUES ('2', '15');
INSERT INTO `oto_dealer_auth_group_access` VALUES ('4', '12');
INSERT INTO `oto_dealer_auth_group_access` VALUES ('5', '13');
INSERT INTO `oto_dealer_auth_group_access` VALUES ('6', '13');
INSERT INTO `oto_dealer_auth_group_access` VALUES ('7', '13');

-- ----------------------------
-- Table structure for `oto_dealer_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `oto_dealer_auth_rule`;
CREATE TABLE `oto_dealer_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=224 DEFAULT CHARSET=utf8 COMMENT='规则表';

-- ----------------------------
-- Records of oto_dealer_auth_rule
-- ----------------------------
INSERT INTO `oto_dealer_auth_rule` VALUES ('1', '20', 'Dealer/ShowNav/nav', '菜单管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('2', '1', 'Dealer/Nav/index', '菜单列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('3', '1', 'Dealer/Nav/add', '添加菜单', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('4', '1', 'Dealer/Nav/edit', '修改菜单', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('5', '1', 'Dealer/Nav/delete', '删除菜单', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('21', '0', 'Dealer/ShowNav/rule', '权限控制', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('7', '21', 'Dealer/Rule/index', '权限管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('8', '7', 'Dealer/Rule/add', '添加权限', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('9', '7', 'Dealer/Rule/edit', '修改权限', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('10', '7', 'Dealer/Rule/delete', '删除权限', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('11', '21', 'Dealer/Rule/group', '部门管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('12', '11', 'Dealer/Rule/add_group', '添加部门', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('13', '11', 'Dealer/Rule/edit_group', '修改部门', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('14', '11', 'Dealer/Rule/delete_group', '删除部门', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('15', '11', 'Dealer/Rule/rule_group', '分配权限', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('16', '11', 'Dealer/Rule/check_user', '添加成员', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('19', '21', 'Dealer/Rule/admin_user_list', '管理员列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('20', '0', 'Dealer/ShowNav/config', '系统设置', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('6', '0', 'Dealer/Index/index', '后台首页', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('64', '1', 'Dealer/Nav/order', '菜单排序', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('96', '6', 'Dealer/Index/welcome', '欢迎界面', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('222', '132', 'Dealer/Product/putaway', '上架线路', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('221', '166', 'Dealer/Finance/exportGroupOrderList', '导出团号订单详情', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('219', '132', 'Dealer/Line/add', '发布线路', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('218', '217', 'Dealer/Product/exportExcelGroupList', '导出excel', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('217', '131', 'Dealer/Product/groupManage', '团号管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('216', '132', 'Dealer/Product/addLinePartner', '线路新增合作运营商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('215', '132', 'Dealer/EditLine/form_step1', '线路编辑', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('214', '132', 'Dealer/Product/auditLine', '线路审核', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('213', '132', 'Dealer/Product/partnersByLineId', '线路合作运营商列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('212', '132', 'Dealer/Product/soldOut', '下架线路', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('123', '11', 'Dealer/Rule/add_user_to_group', '设置为管理员', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('136', '135', 'Dealer/Rule/add_admin', '添加员工', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('137', '135', 'Dealer/Rule/edit_admin', '修改员工', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('131', '0', 'Dealer/ShowNav/Product', '产品管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('132', '131', 'Dealer/Product/lineList', '线路管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('211', '132', 'Dealer/Product/lineDetail', '线路详情', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('135', '0', '', '员工管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('138', '135', 'Dealer/Rule/changeUserStatus', '启用/禁用员工', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('140', '0', 'Dealer/ShowNav/ajax_upload_file', '其它权限', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('141', '140', 'Dealer/Base/ajax_upload_file', '文件上传', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('142', '20', 'Dealer/Platform/editPlatformInfo', '基本资料', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('143', '20', 'Dealer/Platform/editPassword', '修改密码', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('144', '0', 'Dealer/City', '城市管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('145', '144', 'Dealer/City/country', '国家列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('146', '144', 'Dealer/City/province', '省列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('147', '144', 'Dealer/City/delCity', '删除地区', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('148', '144', 'Dealer/City/editCity', '编辑地区', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('149', '145', 'Dealer/City/addCountry', '添加国家', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('150', '146', 'Dealer/City/addCity', '添加省市区', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('151', '145', 'Dealer/City/provinceByCountry', '国家下面省份', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('152', '146', 'Dealer/City/cityByProvince', '省下面市区', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('153', '146', 'Dealer/City/areaByCity', '市下面区县', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('154', '0', 'Dealer/Visa', '签证管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('155', '154', 'Dealer/Visa/visaList', '签证列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('156', '155', 'Dealer/Visa/addVisa', '签证添加', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('157', '155', 'Dealer/Visa/editVisa', '签证修改', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('158', '155', 'Dealer/Visa/visaStatus', '签证上下架', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('159', '0', 'Dealer/Orders', '订单管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('160', '159', 'Dealer/Orders/orderList', '订单列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('161', '160', 'Dealer/Orders/detail', '订单详情', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('162', '0', 'Dealer/Finance', '财务管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('163', '162', 'Dealer/Finance/financeList', '财务结算', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('164', '162', 'Dealer/Finance/operationList', '运营商结算', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('165', '162', 'Dealer/Finance/report', '营业收入报表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('166', '163', 'Dealer/Finance/groupOrderDetail', '团号订单详情', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('167', '0', 'Dealer/Shops', '商家管理', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('168', '167', 'Dealer/Shops/addShops', '添加供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('169', '167', 'Dealer/Shops/trafficShops', '交通供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('170', '167', 'Dealer/Shops/scenicShops', '景区供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('171', '167', 'Dealer/Shops/insureShops', '保险供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('172', '167', 'Dealer/Shops/hotelShops', '酒店供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('173', '169', 'Dealer/Shops/trafficExportExcel', '导出交通供应商列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('174', '169', 'Dealer/Shops/trafficStatusById', '[锁定/开通]交通供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('175', '169', 'Dealer/Shops/trafficPartnerRecord', '合作记录', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('176', '169', 'Dealer/Shops/trafficProductById', '供应产品详情', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('177', '172', 'Dealer/Shops/hotelExportExcel', '导出酒店供应商列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('178', '172', 'Dealer/Shops/addHotelRoom', '新增酒店房间', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('179', '172', 'Dealer/Shops/hotelStatusById', '酒店供应商状态锁定', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('180', '172', 'Dealer/Shops/hotelProductById', '酒店供应商产品列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('181', '172', 'Dealer/Shops/hotelPartnerRecord', '合作记录', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('182', '172', 'Dealer/Shops/editHotelRoom', '酒店房间审核', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('183', '172', 'Dealer/Shops/hotelRoomShelvesById', '酒店房间上下架', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('184', '172', 'Dealer/Shops/hotelExportExcelBySupplierId', '导出酒店供应商产品列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('185', '169', 'Dealer/Shops/editTraffic', '线路审核', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('186', '169', 'Dealer/Shops/trafficShelvesById', '线路上下架', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('187', '169', 'Dealer/Shops/trafficExportExcelBySupplierId', '导出供应商线路列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('188', '169', 'Dealer/Shops/ajaxGetSeatByTrafficId', '获取交通座位列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('189', '170', 'Dealer/Shops/addScenic', '添加景区', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('190', '170', 'Dealer/Shops/scenicStatusById', '[锁定/开通]景区供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('191', '170', 'Dealer/Shops/scenicProductById', '景区供应商产品列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('192', '170', 'Dealer/Shops/scenicPartnerRecord', '合作记录', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('193', '170', 'Dealer/Shops/scenicExportExcel', '导出景区供应商列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('194', '170', 'Dealer/Shops/scenicExportExcelBySupplierId', '导出景区供应商产品列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('195', '170', 'Dealer/Shops/editScenic', '景点审核', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('196', '170', 'Dealer/Shops/scenicShelvesById', '景点上下架', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('197', '171', 'Dealer/Shops/addInsure', '添加保险', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('198', '171', 'Dealer/Shops/insureProductById', '保险供应商产品列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('199', '171', 'Dealer/Shops/insureStatusById', '[锁定/开通]保险供应商', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('200', '171', 'Dealer/Shops/insurePartnerRecord', '合作记录', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('201', '171', 'Dealer/Shops/insureExportExcel', '导出保险供应商列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('202', '171', 'Dealer/Shops/editInsure', '保险审核', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('203', '171', 'Dealer/Shops/insureShelvesById', '保险上下架', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('204', '171', 'Dealer/Shops/insureExportExcelBySupplierId', '导出保险商产品列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('205', '163', 'Dealer/Finance/exportExcelFinanceList', '导出财务结算列表', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('206', '163', 'Dealer/Finance/groupWindup', '[发起/确认]结算', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('207', '163', 'Dealer/Product/groupDetail', '详情', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('208', '164', 'Dealer/Finance/exportExcelOperationList', '导出EXCEL', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('209', '165', 'Dealer/Finance/exportExcelReport', '导出EXCEL', '1', '1', '');
INSERT INTO `oto_dealer_auth_rule` VALUES ('210', '160', 'Dealer/Orders/exportOrderList', '导出EXCEL', '1', '1', '');

-- ----------------------------
-- Table structure for `oto_operator`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator`;
CREATE TABLE `oto_operator` (
  `operator_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '运营商ID',
  `operator_sn` varchar(20) NOT NULL DEFAULT '' COMMENT '运营商编号',
  `login_secret` int(11) DEFAULT NULL COMMENT '登录密码加密参数',
  `login_pwd` varchar(50) NOT NULL DEFAULT '' COMMENT '登录密码',
  `operator_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '运营商类型(1国内、2国外）',
  `landline_number` varchar(20) NOT NULL DEFAULT '' COMMENT '座机号码',
  `operator_account` varchar(20) NOT NULL DEFAULT '' COMMENT '运营商账号',
  `province_id` int(11) NOT NULL DEFAULT '0' COMMENT '省ID',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '市ID',
  `area_id` int(11) NOT NULL DEFAULT '0' COMMENT '区ID',
  `detailed_address` varchar(255) NOT NULL DEFAULT '' COMMENT '具体街道等详细地址',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `operator_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态正常1 ,异常0',
  `operator_flag` tinyint(4) NOT NULL DEFAULT '1' COMMENT ' 正常1,0被删除',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '0为系统管理员, 其它为pid的员工',
  `operator_phone` varchar(20) DEFAULT NULL COMMENT ' 手机号',
  `operator_name` varchar(60) DEFAULT NULL COMMENT ' 姓名',
  `currency_id` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '使用的币种,关联currency表currency_id,默认1为人民币',
  `is_store` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '0不是门店,1门店',
  `store_name` char(50) DEFAULT '' COMMENT '运营商直营门店名称',
  PRIMARY KEY (`operator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oto_operator
-- ----------------------------
INSERT INTO `oto_operator` VALUES ('3', '', '5691', 'c384c60e438ef647bbea52d0a9bbc79f', '0', '', 'admin', '0', '0', '0', '', '1504514041', '1', '1', '0', '18600008881', '总公司', '1', '0', '总公司');

-- ----------------------------
-- Table structure for `oto_operator_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator_auth_group`;
CREATE TABLE `oto_operator_auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text COMMENT '规则id',
  `admin_id` int(11) unsigned NOT NULL COMMENT '所属哪一个管理员ID',
  `is_store` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '1是，-1不是;是否属于门店类型',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- ----------------------------
-- Records of oto_operator_auth_group
-- ----------------------------
INSERT INTO `oto_operator_auth_group` VALUES ('11', '超级管理员', '1', '6,96,20,1,2,3,4,5,64,21,7,8,9,10,11,12,13,14,15,16,123,124,125,19,104,105,106,107,108,118,109,110,111,112,117,131,132', '1', '-1');
INSERT INTO `oto_operator_auth_group` VALUES ('9', '超级管理员', '1', '6,96', '3', '-1');
INSERT INTO `oto_operator_auth_group` VALUES ('2', '产品管理员', '1', '6,96,1,2,3,4,56,57,60,61,63,71,72,65,67,74,75,66,68,69,70,73,77,78,82,83,88,89,90,99,91,92,97,98,104,105,106,107,108,118,109,110,111,112,117,113,114', '1', '-1');
INSERT INTO `oto_operator_auth_group` VALUES ('10', '员工列表', '1', '6,96,165,166,167', '3', '-1');
INSERT INTO `oto_operator_auth_group` VALUES ('6', '员工', '1', '6,96', '0', '-1');
INSERT INTO `oto_operator_auth_group` VALUES ('8', '文章编辑', '1', '104,105,106,107,108,118,109,110,111,112,117', '1', '-1');
INSERT INTO `oto_operator_auth_group` VALUES ('12', '天河分店', '1', null, '1', '1');
INSERT INTO `oto_operator_auth_group` VALUES ('13', '海珠分店', '1', null, '1', '1');
INSERT INTO `oto_operator_auth_group` VALUES ('14', '店长', '1', null, '3', '1');
INSERT INTO `oto_operator_auth_group` VALUES ('15', 'ZHANG', '1', null, '3', '1');

-- ----------------------------
-- Table structure for `oto_operator_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator_auth_group_access`;
CREATE TABLE `oto_operator_auth_group_access` (
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `group_id` int(11) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

-- ----------------------------
-- Records of oto_operator_auth_group_access
-- ----------------------------
INSERT INTO `oto_operator_auth_group_access` VALUES ('1', '11');
INSERT INTO `oto_operator_auth_group_access` VALUES ('3', '9');
INSERT INTO `oto_operator_auth_group_access` VALUES ('4', '10');
INSERT INTO `oto_operator_auth_group_access` VALUES ('5', '10');
INSERT INTO `oto_operator_auth_group_access` VALUES ('6', '10');
INSERT INTO `oto_operator_auth_group_access` VALUES ('6', '11');
INSERT INTO `oto_operator_auth_group_access` VALUES ('7', '12');
INSERT INTO `oto_operator_auth_group_access` VALUES ('7', '14');
INSERT INTO `oto_operator_auth_group_access` VALUES ('8', '12');
INSERT INTO `oto_operator_auth_group_access` VALUES ('9', '13');

-- ----------------------------
-- Table structure for `oto_operator_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator_auth_rule`;
CREATE TABLE `oto_operator_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=248 DEFAULT CHARSET=utf8 COMMENT='规则表';

-- ----------------------------
-- Records of oto_operator_auth_rule
-- ----------------------------
INSERT INTO `oto_operator_auth_rule` VALUES ('1', '20', 'operator/ShowNav/nav', '菜单管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('2', '1', 'operator/Nav/index', '菜单列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('3', '1', 'operator/Nav/add', '添加菜单', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('4', '1', 'operator/Nav/edit', '修改菜单', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('5', '1', 'operator/Nav/delete', '删除菜单', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('21', '0', 'operator/ShowNav/rule', '权限控制', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('7', '21', 'operator/Rule/index', '权限管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('8', '7', 'operator/Rule/add', '添加权限', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('9', '7', 'operator/Rule/edit', '修改权限', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('10', '7', 'operator/Rule/delete', '删除权限', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('11', '21', 'operator/Rule/group', '用户组管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('12', '11', 'operator/Rule/add_group', '添加用户组', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('13', '11', 'operator/Rule/edit_group', '修改用户组', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('14', '11', 'operator/Rule/delete_group', '删除用户组', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('15', '11', 'operator/Rule/rule_group', '分配权限', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('16', '11', 'operator/Rule/check_user', '添加成员', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('19', '21', 'operator/Rule/admin_user_list', '管理员列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('20', '0', 'operator/ShowNav/config', '系统设置', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('6', '0', 'operator/Index/index', '后台首页', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('64', '1', 'operator/Nav/order', '菜单排序', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('96', '6', 'operator/Index/welcome', '欢迎界面', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('143', '142', 'Operator/Shops/addShops', '添加商家', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('142', '0', 'Operator/UnitShops', '单品供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('141', '137', 'Operator/Product/cooperationList', '线路合作申请', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('140', '137', 'Operator/Product/groupManage', '团号管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('139', '137', 'Operator/Product/lineList', '线路列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('138', '137', 'Operator/Line/add', '自营线路发布', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('137', '0', 'Operator/Product/index', '产品管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('136', '133', 'Operator/Visitor/grade_config', '等级设置', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('135', '133', 'Operator/Score/score_config', '积分设置', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('134', '133', 'Operator/Visitor/visitor_list', '会员列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('133', '0', 'Operator/ShowNav/Visitor', '会员管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('123', '11', 'operator/Rule/add_user_to_group', '设置为管理员', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('124', '19', 'operator/Rule/add_admin', '添加管理员', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('125', '19', 'operator/Rule/edit_admin', '修改管理员', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('184', '19', 'Operator/Rule/changeUserStatus', '[禁用/启用]管理员', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('131', '0', 'Operator/Finance', '财务管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('132', '131', 'Operator/Finance/bill_list', '发票管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('144', '142', 'Operator/Shops/trafficShops', '交通供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('145', '142', 'Operator/Shops/hotelShops', '酒店供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('146', '142', 'Operator/Shops/scenicShops', '景区供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('147', '142', 'Operator/Shops/insureShops', '保险供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('148', '0', 'Operator/Visa', '签证管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('149', '148', 'Operator/Visa/addVisa', '添加签证', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('150', '148', 'Operator/Visa/visaList', '签证列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('151', '0', 'Operator/City', '城市管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('152', '151', 'Operator/City/country', '国家列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('153', '151', 'Operator/City/province', '省列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('154', '131', 'Operator/Finance/settlement_list', '销售订单结算', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('155', '131', 'Operator/Finance/reseller_list', '分销结算列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('156', '131', 'Operator/Finance/performance_list', '员工销售业绩', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('157', '131', 'Operator/Report/businessStatement', '营业收入报表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('158', '131', 'Operator/Report/selfIncome', '自营收入报表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('159', '131', 'Operator/Report/financialManagement', '财务结算', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('160', '0', 'Operator/Shops', '商家管理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('161', '160', 'Operator/Shops/lineShops', '注册供应商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('162', '161', 'Operator/Shops/addLineShops', '[添加/编辑]注册供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('163', '164', 'Operator/Shops/addResellerShops', '[添加/编辑]分销商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('164', '160', 'Operator/Shops/resellerShops', '分销商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('168', '134', 'Operator/Visitor/add_visitor', '添加会员', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('169', '134', 'Operator/Visitor/changeVisitorStatusById', '更改会员状态', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('170', '134', 'Operator/Visitor/visitorExportExcel', '导出会员列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('171', '150', 'Operator/Visa/editVisa', '编辑签证', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('172', '150', 'Operator/Visa/visaStatus', '[上架/下架]签证', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('173', '132', 'Operator/Finance/outBillListExportExcel', '导出发票列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('174', '154', 'Operator/Finance/outSettlementListExportExcel', '导出销售订单结算列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('175', '154', 'Operator/Index/orderDetail', '订单详情', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('176', '154', 'Operator/Finance/singleSettlementById', '确认结算', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('177', '154', 'Operator/Finance/batchSettlement', '批量确认结算', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('178', '155', 'Operator/Finance/outResellerSettlementListExportExcel', '导出分销商结算列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('179', '155', 'Operator/Finance/refund', '退款处理', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('180', '156', 'Operator/Finance/outPerformanceExcel', '导出员工销售业绩列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('181', '157', 'Operator/Report/outBusinessStatement', '导出营业收入总报表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('182', '158', 'Operator/Report/outSelfIncome', '导出自营收入报表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('183', '159', 'Operator/Report/outFinancialManagement', '导出财务管理列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('185', '139', 'Operator/Product/specialLineDetail', '专线线路详情', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('186', '139', 'Operator/Product/specialLineDetaillineDetail', '自营线路详情', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('187', '139', 'Operator/Product/soldOut', '线路下架', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('188', '139', 'Operator/Product/soldOutputaway', '线路上架', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('189', '139', 'Operator/SpecialLine/form_step1', '专线线路编辑', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('190', '139', 'Operator/EditLine/form_step1', '自营线路编辑', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('191', '139', 'Operator/Product/auditSpecialLine', '专线线路审核', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('192', '139', 'Operator/Product/auditLine', '自营线路审核', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('193', '140', 'Operator/Product/groupDetail', '团号订单列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('194', '140', 'Operator/Product/exportExcelGroupList', '导出团号列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('195', '140', 'Operator/Product/detail', '团号订单详情', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('196', '141', 'Operator/Product/specialLineDetailsupplierLineDetail', '未审核线路详情', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('197', '141', 'Operator/Product/cooperationHandle', '[同意/拒绝]线路合作', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('216', '144', 'Operator/Shops/trafficExportExcel', '导出交通供应商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('199', '144', 'Operator/Shops/addTraffic', '添加交通产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('200', '144', 'Operator/Shops/trafficStatusById', '[开通/锁定]交通供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('201', '144', 'Operator/Shops/trafficProductById', '交通供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('202', '144', 'Operator/Shops/trafficPartnerRecord', '交通供应商合作记录', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('203', '144', 'Operator/Shops/trafficExportExcelBySupplierId', '导出交通供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('204', '144', 'Operator/Shops/editTraffic', '交通供应商产品审核', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('205', '144', 'Operator/Shops/trafficShelvesById', '[上架/下架]产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('206', '144', 'Operator/Shops/exportTrafficPartnerRecord', '导出合作记录', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('207', '145', 'Operator/Shops/hotelExportExcel', '导出酒店供应商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('208', '145', 'Operator/Shops/addHotelRoom', '添加酒店产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('209', '145', 'Operator/Shops/hotelStatusById', '[开通/锁定]酒店供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('210', '145', 'Operator/Shops/hotelProductById', '酒店供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('211', '145', 'Operator/Shops/hotelPartnerRecord', '酒店供应商合作记录', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('212', '145', 'Operator/Shops/hotelExportExcelBySupplierId', '导出酒店供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('213', '145', 'Operator/Shops/editHotelRoom', '酒店供应商产品审核', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('214', '145', 'Operator/Shops/hotelRoomShelvesById', '[上架/下架]酒店供应商产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('215', '145', 'Operator/Shops/exportHotelPartnerRecord', '导出酒店供应商合作记录列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('217', '146', 'Operator/Shops/scenicExportExcel', '导出景区供应商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('218', '146', 'Operator/Shops/addScenic', '添加景区产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('219', '146', 'Operator/Shops/scenicStatusById', '[锁定/开通]景区供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('220', '146', 'Operator/Shops/scenicProductById', '景区供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('221', '146', 'Operator/Shops/scenicPartnerRecord', '景区供应商合作记录', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('222', '146', 'Operator/Shops/scenicExportExcelBySupplierId', '导出景区供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('223', '146', 'Operator/Shops/editScenic', '景区供应商产品审核', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('224', '146', 'Operator/Shops/scenicShelvesById', '[上架/下架]景区供应商产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('225', '146', 'Operator/Shops/exportScenicPartnerRecord', '导出景区供应商合作记录列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('226', '147', 'Operator/Shops/insureExportExcel', '导出保险供应商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('227', '147', 'Operator/Shops/addInsure', '添加保险产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('228', '147', 'Operator/Shops/insureProductById', '供应供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('229', '147', 'Operator/Shops/insureStatusById', '[锁定/开通]保险供应商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('230', '147', 'Operator/Shops/insurePartnerRecord', '保险供应商合作记录', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('231', '147', 'Operator/Shops/insureExportExcelBySupplierId', '导出保险供应商产品列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('232', '147', '1Operator/Shops/editInsure', '保险供应商产品审核', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('233', '147', 'Operator/Shops/insureShelvesById', '[上架/下架]保险供应商产品', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('234', '151', 'Operator/City/delCity', '删除地区', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('235', '151', 'Operator/City/editCity', '编辑地区', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('236', '152', 'Operator/City/addCountry', '添加国家', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('237', '152', 'Operator/City/provinceByCountry', '国家下面省份', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('238', '153', 'Operator/City/addCity', '添加省市区', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('239', '153', 'Operator/City/cityByProvince', '省下面市区', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('240', '153', 'Operator/City/areaByCity', '市下面区县', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('241', '161', 'Operator/Shops/changeSupplierStatusById', '[锁定/开通]帐号', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('242', '161', 'Operator/Shops/lineShopsExportExcel', '导出供应商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('243', '164', 'Operator/Shops/changeresellerStatusById', '[锁定/开通]分销商', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('244', '164', 'Operator/Shops/lineResellerExportExcel', '导出分销商列表', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('245', '131', 'Operator/Finance/financeList', '供应商线路结算', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('246', '245', 'Operator/Finance/exportGroupCostDetail', '导出结算明细', '1', '1', '');
INSERT INTO `oto_operator_auth_rule` VALUES ('247', '245', 'Operator/Finance/closingStatus', '确认结算', '1', '1', '');

-- ----------------------------
-- Table structure for `oto_operator_line_reseller`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator_line_reseller`;
CREATE TABLE `oto_operator_line_reseller` (
  `reseller_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分销商ID',
  `reseller_sn` varchar(255) DEFAULT '' COMMENT '分销商编号(可登陆)',
  `login_secret` int(11) DEFAULT NULL,
  `login_pwd` varchar(50) NOT NULL DEFAULT '',
  `money_type` varchar(255) DEFAULT '人民币' COMMENT '币种',
  `reseller_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '分销商类型(国内旅行社1、国外旅行社2）',
  `landline_tel` varchar(255) DEFAULT NULL COMMENT '座机号码，不能是手机号码',
  `reseller_account` varchar(255) NOT NULL DEFAULT '' COMMENT '分销商账号',
  `reseller_name` varchar(255) NOT NULL DEFAULT '' COMMENT '分销商真实姓名',
  `reseller_phone` char(11) NOT NULL DEFAULT '' COMMENT '分销商手机号码',
  `reseller_gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别（1男，2女）',
  `reseller_status` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '1正常，-1锁定',
  `is_selfScore` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '是否自营积分(默认否)1正常，-1否',
  `web_name` varchar(255) DEFAULT NULL COMMENT '网站名称',
  `web_url` varchar(255) DEFAULT NULL COMMENT '网站域名',
  `web_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '网店状态1开启，-1关闭',
  `company_name` varchar(255) DEFAULT NULL COMMENT '公司名称',
  `service_level` tinyint(4) DEFAULT NULL COMMENT '服务等级1~5',
  `is_credible` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '是否可信认证1认证，-1未认证默认',
  `legal_person` varchar(200) DEFAULT NULL COMMENT '法人代表',
  `linkman` varchar(200) DEFAULT NULL COMMENT '联系人',
  `fax_number` varchar(200) DEFAULT NULL COMMENT '传真号码',
  `sales_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '销售类型1直营，2加盟',
  `province_id` int(11) NOT NULL DEFAULT '0' COMMENT '省ID',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '市ID',
  `area_id` int(11) NOT NULL DEFAULT '0' COMMENT '区ID',
  `detailed_address` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `id_number` bigint(20) DEFAULT NULL COMMENT '证件号码',
  `is_certification` tinyint(4) DEFAULT NULL COMMENT '身份证等证件号是否已经认证',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序号',
  `start_time` int(11) DEFAULT NULL COMMENT '合作有效起始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '合作有效结束时间',
  `business_details` varchar(255) DEFAULT NULL COMMENT '公司业务详情',
  `system_remarks` varchar(255) DEFAULT NULL COMMENT '系统备注',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `input_id` int(11) NOT NULL DEFAULT '0' COMMENT '录入人ID',
  `business_img` varchar(255) DEFAULT NULL COMMENT '营业执照，多个逗号隔开',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '级别',
  `operator_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '运营商ID,谁添加的分销商',
  `reseller_flag` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1正常,0删除',
  `is_review` tinyint(4) NOT NULL DEFAULT '0' COMMENT '默认-1未审核，1已审核，-2被拒绝',
  `business_number` varchar(60) NOT NULL DEFAULT '' COMMENT '营业执照编号',
  `country_id` int(11) NOT NULL DEFAULT '0' COMMENT '国家id',
  `qq_email` char(35) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `shop_number` varchar(60) NOT NULL DEFAULT '' COMMENT '门店编号',
  `shop_logo_img` varchar(255) NOT NULL DEFAULT '' COMMENT '店铺logo',
  `id_img` varchar(255) NOT NULL DEFAULT '' COMMENT '身份证正面图片',
  `id_img_reverse` varchar(255) NOT NULL DEFAULT '' COMMENT '身份证反面图片',
  `store_name` varchar(50) DEFAULT '' COMMENT '分销商店铺名称',
  `currency_id` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '币种',
  `shop_or_reseller` tinyint(4) NOT NULL DEFAULT '1' COMMENT ' 1门店 2分销商',
  PRIMARY KEY (`reseller_id`),
  KEY `pid` (`pid`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='运营商线路分销商表';

-- ----------------------------
-- Records of oto_operator_line_reseller
-- ----------------------------
INSERT INTO `oto_operator_line_reseller` VALUES ('1', '10002982', '5691', 'c384c60e438ef647bbea52d0a9bbc79f', '', '1', '18600008883', '18600008883', '分销', '18600008883', '1', '1', '1', '分销', 'www.fenxiao.com', '1', '特惠分销有限公司', '5', '-1', '特惠', '特惠', '13060506060', '1', '210000', '210600', '210602', '笑话', '13060506060', '1', '0', '1510848000', '1854720000', 'sfsadf', '咦', '1615012214', '0', '/Upload/business/2017-11-17/5a0e7f47e1300.jpg', '1', '3', '1', '1', '', '820303', '', '', '', '', '', '', '1', '2');

-- ----------------------------
-- Table structure for `oto_operator_line_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator_line_supplier`;
CREATE TABLE `oto_operator_line_supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '供应商ID',
  `supplier_sn` varchar(255) NOT NULL DEFAULT '' COMMENT '供应商编号(可登陆)',
  `loginSecret` int(11) DEFAULT NULL,
  `loginPwd` varchar(50) NOT NULL,
  `supplier_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '供应商类型(国内1、国外2）',
  `landline_tel` varchar(255) NOT NULL DEFAULT '' COMMENT '座机号码，不能是手机号码',
  `supplier_account` varchar(255) NOT NULL DEFAULT '' COMMENT '供应商账号',
  `supplier_name` varchar(255) NOT NULL DEFAULT '' COMMENT '供应商真实姓名',
  `supplier_phone` char(11) NOT NULL DEFAULT '' COMMENT '供应商手机号码',
  `supplier_gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别（1男，2女）',
  `supplier_status` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '1正常，-1锁定',
  `supplier_code` varchar(255) DEFAULT NULL COMMENT '供应商代码',
  `company_name` varchar(255) DEFAULT NULL COMMENT '公司名称',
  `service_level` tinyint(4) DEFAULT NULL COMMENT '服务等级1~5',
  `is_review` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '审核状态（默认-1未审核，1已审核，-2被拒绝）',
  `legal_person` varchar(200) DEFAULT NULL COMMENT '法人代表',
  `linkman` varchar(200) DEFAULT NULL COMMENT '联系人',
  `fax_number` varchar(200) DEFAULT NULL COMMENT '传真号码',
  `province_id` int(11) NOT NULL DEFAULT '0' COMMENT '省ID',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '市ID',
  `area_id` int(11) NOT NULL DEFAULT '0' COMMENT '区ID',
  `detailed_address` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `id_number` varchar(20) DEFAULT NULL COMMENT '证件号码',
  `is_certification` tinyint(4) DEFAULT NULL COMMENT '身份证等证件号是否已经认证',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序号',
  `start_time` int(10) NOT NULL DEFAULT '0' COMMENT '合作有效起始时间',
  `end_time` int(10) NOT NULL DEFAULT '0' COMMENT '合作有效结束时间',
  `business_details` varchar(255) DEFAULT NULL COMMENT '公司业务详情',
  `is_integral` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '是否享受运营商积分优惠(1,是，-1否)',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `input_id` int(11) NOT NULL DEFAULT '0' COMMENT '录入人ID',
  `audit_id` int(11) NOT NULL DEFAULT '0' COMMENT '审核人ID',
  `business_img` varchar(255) DEFAULT '' COMMENT '营业执照，多个逗号隔开',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '0是超级管理员,其它值为员工',
  `supplier_flag` tinyint(4) unsigned DEFAULT '1' COMMENT '1正常,0删除',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `business_number` varchar(60) DEFAULT NULL COMMENT ' 营业执照编号',
  `qq_email` varchar(60) DEFAULT NULL COMMENT 'qq邮箱',
  `shop_number` varchar(60) DEFAULT NULL COMMENT ' 门店编号',
  `shop_logo_img` varchar(255) DEFAULT NULL COMMENT '店铺logo',
  `id_img` varchar(255) DEFAULT NULL COMMENT '身份证正面图片',
  `id_img_reverse` varchar(255) DEFAULT NULL COMMENT ' 身份证反面图片',
  `operator_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT ' 运营商id,即谁开的供应商',
  `currency_id` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '使用的币种,关联currency表currency_id,默认1为人民币',
  `country_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '国家ID',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='运营商线路提供供应商表';

-- ----------------------------
-- Records of oto_operator_line_supplier
-- ----------------------------
INSERT INTO `oto_operator_line_supplier` VALUES ('1', '10002023', '5691', 'c384c60e438ef647bbea52d0a9bbc79f', '1', '18600008882', '18600008882', '特惠', '18600008882', '2', '1', 'TH', '特惠科技有限公司', '5', '1', 'lin', '13060506050', '13060506050', '440000', '440100', '440106', '天河北路时代广场', '13060506050', '1', '0', '1510848000', '1854720000', '特惠，因人们而生', '-1', '1615012268', '0', '0', '/Upload/business/2017-11-17/5a0e7d3e6728c.jpg', '0', '1', null, null, '', null, null, null, null, '3', '1', '820303');

-- ----------------------------
-- Table structure for `oto_operator_sales_channel`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator_sales_channel`;
CREATE TABLE `oto_operator_sales_channel` (
  `channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `line_id` int(11) NOT NULL DEFAULT '0' COMMENT '线路ID，关联线路表',
  `channel_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否全渠道,0否,1是',
  `store_id` varchar(255) NOT NULL DEFAULT '-1' COMMENT '赋予查看当前线路权限的线下门店id集合，逗号隔开存储默认-1全部门店都没权限',
  `seller_id` varchar(255) NOT NULL DEFAULT '-1' COMMENT '分销商id集合，逗号隔开。默认-1全部都没有权限',
  `platform_id` varchar(255) NOT NULL DEFAULT '' COMMENT '平台分销,APP=1,微商城=2,B2C=3 多个值用,号分开',
  `operator_id` int(11) NOT NULL DEFAULT '0' COMMENT '运营商ID',
  PRIMARY KEY (`channel_id`) COMMENT '修改channel_id为自增不为空的主键'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='运营商销售渠道表';

-- ----------------------------
-- Records of oto_operator_sales_channel
-- ----------------------------
INSERT INTO `oto_operator_sales_channel` VALUES ('1', '2', '0', '2', '1', '', '3');
INSERT INTO `oto_operator_sales_channel` VALUES ('5', '3', '0', '2', '1', '', '3');
INSERT INTO `oto_operator_sales_channel` VALUES ('6', '5', '0', '2', '3', '', '3');
INSERT INTO `oto_operator_sales_channel` VALUES ('7', '9', '0', '2,4', '1,3', '', '3');
INSERT INTO `oto_operator_sales_channel` VALUES ('8', '10', '0', '3,3', '1,3', '', '3');
INSERT INTO `oto_operator_sales_channel` VALUES ('9', '11', '0', '2,4', '1,3', '', '3');
INSERT INTO `oto_operator_sales_channel` VALUES ('10', '12', '0', '3,3', '1,3', '', '3');
INSERT INTO `oto_operator_sales_channel` VALUES ('11', '14', '0', '2', '1', '', '3');

-- ----------------------------
-- Table structure for `oto_operator_special_price`
-- ----------------------------
DROP TABLE IF EXISTS `oto_operator_special_price`;
CREATE TABLE `oto_operator_special_price` (
  `sprice_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '特殊价格ID',
  `adult_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '成人价',
  `child_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '儿童价',
  `oldman_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '老人价',
  `line_id` int(11) NOT NULL DEFAULT '0' COMMENT '线路ID',
  `day` char(10) NOT NULL DEFAULT '' COMMENT '特殊日期时间 存储格式为20170920格式',
  `origin_adult_price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '源线路成人价',
  `origin_child_price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '原儿童成本价',
  `origin_money_type` varchar(60) DEFAULT '人民币' COMMENT '源币种',
  `now_money_type` varchar(60) DEFAULT '人民币' COMMENT '币种',
  `origin_oldman_price` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '源老人成本价',
  PRIMARY KEY (`sprice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='运营商特殊日期价格表';

-- ----------------------------
-- Records of oto_operator_special_price
-- ----------------------------
INSERT INTO `oto_operator_special_price` VALUES ('1', '400.00', '200.00', '0.00', '1', '20171118', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('2', '600.00', '300.00', '0.00', '1', '20171204', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('3', '401.00', '201.00', '0.00', '2', '20171118', '400.00', '200.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('4', '601.00', '301.00', '0.00', '2', '20171204', '600.00', '300.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('13', '600.00', '300.00', '0.00', '3', '20171118', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('14', '600.00', '300.00', '0.00', '3', '20171209', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('17', '400.00', '200.00', '0.00', '6', '20171130', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('18', '400.00', '200.00', '0.00', '6', '20171215', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('19', '1200.00', '800.00', '0.00', '7', '20171211', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('20', '1400.00', '1000.00', '0.00', '9', '20171211', '1200.00', '800.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('22', '800.00', '600.00', '0.00', '11', '20171130', '400.00', '200.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('23', '600.00', '400.00', '0.00', '11', '20171215', '400.00', '200.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('26', '800.00', '400.00', '0.00', '10', '20171210', '0.00', '0.00', '人民币', '人民币', '0.00');
INSERT INTO `oto_operator_special_price` VALUES ('27', '3300.00', '2800.00', '0.00', '12', '20171203', '0.00', '0.00', '人民币', '人民币', '0.00');

-- ----------------------------
-- Table structure for `oto_oplog`
-- ----------------------------
DROP TABLE IF EXISTS `oto_oplog`;
CREATE TABLE `oto_oplog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_content` varchar(1024) NOT NULL DEFAULT '' COMMENT '操作日志内容',
  `log_time` int(11) unsigned DEFAULT NULL COMMENT ' 操作时间',
  `log_ip` varchar(20) DEFAULT NULL COMMENT '操作IP',
  `inputer` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '操作者的Id',
  `operator_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属平台的运营商ID,即pid=0的operator_id',
  `inputer_name` varchar(20) DEFAULT NULL COMMENT '操作者姓名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1219 DEFAULT CHARSET=utf8 COMMENT='运营商操作日志';

-- ----------------------------
-- Records of oto_oplog
-- ----------------------------

-- ----------------------------
-- Table structure for `oto_refund_record`
-- ----------------------------
DROP TABLE IF EXISTS `oto_refund_record`;
CREATE TABLE `oto_refund_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `refund_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单拒绝退款记录表';

-- ----------------------------
-- Records of oto_refund_record
-- ----------------------------

-- ----------------------------
-- Table structure for `oto_reseller_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `oto_reseller_auth_group`;
CREATE TABLE `oto_reseller_auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text COMMENT '规则id',
  `admin_id` int(11) unsigned NOT NULL COMMENT '所属哪一个管理员ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- ----------------------------
-- Records of oto_reseller_auth_group
-- ----------------------------
INSERT INTO `oto_reseller_auth_group` VALUES ('1', '超级管理员', '1', '6,96,20,1,2,3,4,5,64', '1');
INSERT INTO `oto_reseller_auth_group` VALUES ('9', '超级管理员', '1', '6,96', '3');
INSERT INTO `oto_reseller_auth_group` VALUES ('2', '产品管理员', '1', '6,96,1,2,3,4,56,57,60,61,63,71,72,65,67,74,75,66,68,69,70,73,77,78,82,83,88,89,90,99,91,92,97,98,104,105,106,107,108,118,109,110,111,112,117,113,114', '1');
INSERT INTO `oto_reseller_auth_group` VALUES ('10', '员工列表', '1', '1,2,8,10,3,20,4,5,6', '3');
INSERT INTO `oto_reseller_auth_group` VALUES ('6', '员工1', '1', '6,96,1,2,3,4,5,64,21,7,8,9,10,11,12,13,14,15,16,123,124,125,19', '0');
INSERT INTO `oto_reseller_auth_group` VALUES ('8', '文章编辑', '1', '104,105,106,107,108,118,109,110,111,112,117', '1');
INSERT INTO `oto_reseller_auth_group` VALUES ('13', '员工', '1', '1,2,8', '28');
INSERT INTO `oto_reseller_auth_group` VALUES ('14', '店长', '1', '1,2,8,10', '28');
INSERT INTO `oto_reseller_auth_group` VALUES ('15', '员工', '1', '1,2,8', '34');
INSERT INTO `oto_reseller_auth_group` VALUES ('16', '员工', '1', '1,2,8', '42');
INSERT INTO `oto_reseller_auth_group` VALUES ('17', '员工', '1', '1,2,8', '46');
INSERT INTO `oto_reseller_auth_group` VALUES ('18', '店员', '1', '1,2,8', '48');

-- ----------------------------
-- Table structure for `oto_reseller_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `oto_reseller_auth_group_access`;
CREATE TABLE `oto_reseller_auth_group_access` (
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `group_id` int(11) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

-- ----------------------------
-- Records of oto_reseller_auth_group_access
-- ----------------------------
INSERT INTO `oto_reseller_auth_group_access` VALUES ('3', '6');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('4', '6');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('7', '10');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('8', '6');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('29', '10');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('30', '10');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('31', '10');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('32', '13');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('33', '14');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('35', '15');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('43', '16');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('47', '17');
INSERT INTO `oto_reseller_auth_group_access` VALUES ('49', '18');

-- ----------------------------
-- Table structure for `oto_reseller_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `oto_reseller_auth_rule`;
CREATE TABLE `oto_reseller_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='规则表';

-- ----------------------------
-- Records of oto_reseller_auth_rule
-- ----------------------------
INSERT INTO `oto_reseller_auth_rule` VALUES ('1', '0', 'Reseller/Index/welcome', '首页', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('2', '0', 'Reseller/Sale', '销售管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('3', '0', 'Reseller/System', '系统设置', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('4', '0', 'Reseller/Nav', '权限控制', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('5', '0', 'Reseller/Staff', '员工管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('6', '0', 'Reseller/User', '会员管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('7', '0', 'Reseller/Finance', '财务管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('8', '2', 'Reseller/orderCenter/index', '订购中心', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('10', '2', 'Reseller/SaleManger/OrderList', '订单列表', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('19', '2', 'Reseller/SaleManger/orderRecovery', '订单回收站', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('20', '3', 'Reseller/Nav/index', '菜单管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('21', '3', 'Reseller/Platform/editPlatformInfo', '基本资料', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('22', '3', 'Reseller/Platform/editPassword', '修改密码', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('23', '4', 'Reseller/Rule/index', '权限管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('24', '4', 'Reseller/Rule/group', '部门管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('25', '4', 'Reseller/Rule/admin_user_list/staff', '管理员列表', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('26', '5', 'Reseller/Rule/add_admin', '添加员工', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('27', '5', 'Reseller/Rule/admin_user_list/user', '员工列表', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('28', '6', 'Reseller/Visitor/add_visitor', '会员添加', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('29', '6', 'Reseller/Visitor/visitor_list', '会员列表', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('30', '6', 'Reseller/Score/score_config', '积分设置', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('31', '6', 'Reseller/Visitor/grade_config', '等级设置', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('32', '7', 'Reseller/Finance/billList', '发票管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('33', '7', 'Reseller/Finance/resellerList', '财务结算', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('34', '7', 'Reseller/Finance/report', '营业收入报表', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('35', '10', 'Reseller/SaleManger/confirmPay', '确认订单', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('36', '10', 'Reseller/SaleManger/cancel', '取消订单', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('37', '10', 'Reseller/SaleManger/refuse', '订单退款', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('38', '10', 'Reseller/SaleManger/exportExcels', '订单导出', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('39', '19', 'Reseller/SaleManger/orderRecoveryExportExcels', '导出订单回收站订单', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('40', '32', 'Reseller/Finance/outBillListExportExcel', '导出发票管理', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('42', '33', 'Reseller/Finance/outResellerSettlementListExportExcel', '导出财务结算订单', '1', '1', '');
INSERT INTO `oto_reseller_auth_rule` VALUES ('43', '34', 'Reseller/Finance/exportReportToExcel', '导出营业收入报表', '1', '1', '');

-- ----------------------------
-- Table structure for `oto_reseller_nav`
-- ----------------------------
DROP TABLE IF EXISTS `oto_reseller_nav`;
CREATE TABLE `oto_reseller_nav` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单表',
  `pid` int(11) unsigned DEFAULT '0' COMMENT '所属菜单',
  `name` varchar(15) DEFAULT '' COMMENT '菜单名称',
  `mca` varchar(255) DEFAULT '' COMMENT '模块、控制器、方法',
  `ico` varchar(20) DEFAULT '' COMMENT 'font-awesome图标',
  `order_number` int(11) unsigned DEFAULT NULL COMMENT '排序',
  `platform` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '所属平台 1经销商,2运营商,3分销商',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oto_reseller_nav
-- ----------------------------
INSERT INTO `oto_reseller_nav` VALUES ('1', '0', '系统设置', 'Reseller/System', 'cog', '7', '3');
INSERT INTO `oto_reseller_nav` VALUES ('2', '1', '菜单管理', 'Reseller/Nav/index', null, null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('7', '4', '权限管理', 'Reseller/Rule/index', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('4', '0', '权限控制', 'Reseller/Nav', 'expeditedssl', '6', '3');
INSERT INTO `oto_reseller_nav` VALUES ('8', '4', '部门管理', 'Reseller/Rule/group', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('9', '4', '管理员列表', 'Reseller/Rule/admin_user_list', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('44', '0', '员工管理', 'Reseller/Staff', 'user', '4', '3');
INSERT INTO `oto_reseller_nav` VALUES ('45', '44', '添加员工', 'Reseller/Rule/add_admin', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('47', '44', '员工列表', 'Reseller/Rule/admin_user_list', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('48', '1', '基本资料', 'Reseller/Platform/editPlatformInfo', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('49', '1', '修改密码', 'Reseller/Platform/editPassword', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('84', '0', '会员管理', 'Reseller/User', 'users', '5', '3');
INSERT INTO `oto_reseller_nav` VALUES ('85', '84', '会员添加', 'Reseller/Visitor/add_visitor', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('86', '84', '会员列表', 'Reseller/Visitor/visitor_list', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('87', '84', '积分设置', 'Reseller/Score/score_config', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('88', '84', '等级设置', 'Reseller/Visitor/grade_config', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('89', '0', '财务管理', 'Reseller/Finance', 'cny', '3', '3');
INSERT INTO `oto_reseller_nav` VALUES ('90', '89', '发票管理', 'Reseller/Finance/billList', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('81', '0', '销售管理', 'Reseller/Sale', 'credit-card', '2', '3');
INSERT INTO `oto_reseller_nav` VALUES ('82', '81', '订购中心', 'Reseller/orderCenter/index', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('83', '81', '订单列表', 'Reseller/SaleManger/OrderList', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('91', '89', '财务结算', 'Reseller/Finance/resellerList', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('80', '0', '首页', 'Reseller/Index/welcome', 'home', '1', '3');
INSERT INTO `oto_reseller_nav` VALUES ('92', '89', '营业收入报表', 'Reseller/Finance/report', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('93', '81', '订单回收站', 'Reseller/SaleManger/orderRecovery', '', null, '3');
INSERT INTO `oto_reseller_nav` VALUES ('103', '81', '退款列表', 'Reseller/SaleManger/refundList', '', null, '3');

-- ----------------------------
-- Table structure for `oto_supplier_nav`
-- ----------------------------
DROP TABLE IF EXISTS `oto_supplier_nav`;
CREATE TABLE `oto_supplier_nav` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单表',
  `pid` int(11) unsigned DEFAULT '0' COMMENT '所属菜单',
  `name` varchar(15) DEFAULT '' COMMENT '菜单名称',
  `mca` varchar(255) DEFAULT '' COMMENT '模块、控制器、方法',
  `ico` varchar(20) DEFAULT '' COMMENT 'font-awesome图标',
  `order_number` int(11) unsigned DEFAULT NULL COMMENT '排序',
  `platform` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '所属平台 1经销商,2运营商,3分销商',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oto_supplier_nav
-- ----------------------------
INSERT INTO `oto_supplier_nav` VALUES ('1', '0', '系统设置', 'Dealer/ShowNav/config', 'cog', '7', '1');
INSERT INTO `oto_supplier_nav` VALUES ('2', '1', '菜单管理', 'Dealer/Nav/index', null, null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('7', '4', '权限管理', 'Dealer/Rule/index', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('4', '0', '权限控制', 'Dealer/ShowNav/rule', 'expeditedssl', '6', '1');
INSERT INTO `oto_supplier_nav` VALUES ('8', '4', '部门管理', 'Dealer/Rule/group', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('9', '4', '管理员列表', 'Dealer/Rule/admin_user_list', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('77', '39', '团号管理', 'Dealer/Product/groupManage', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('39', '0', '产品管理', 'Dealer/ShowNav/product', 'tags', '1', '1');
INSERT INTO `oto_supplier_nav` VALUES ('40', '39', '线路管理', 'Dealer/Product/lineList', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('44', '0', '员工管理', 'Dealer/Rule/admin_user_list', 'user', '5', '1');
INSERT INTO `oto_supplier_nav` VALUES ('45', '44', '添加员工', 'Dealer/Rule/add_admin', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('47', '44', '员工列表', 'Dealer/Rule/admin_user_list', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('48', '1', '基本资料', 'Dealer/Platform/editPlatformInfo', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('49', '1', '修改密码', 'Dealer/Platform/editPassword', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('50', '0', '城市管理', 'Dealer/City/country', 'map-marker', '8', '1');
INSERT INTO `oto_supplier_nav` VALUES ('52', '50', '国家列表', 'Dealer/City/country', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('53', '50', '省列表', 'Dealer/City/province', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('54', '0', '签证管理', 'Dealer/Visa', 'pagelines', '6', '1');
INSERT INTO `oto_supplier_nav` VALUES ('57', '0', '订单管理', 'Dealer/Orders', 'puzzle-piece', '2', '1');
INSERT INTO `oto_supplier_nav` VALUES ('55', '54', '签证添加', 'Dealer/Visa/addVisa', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('56', '54', '签证列表', 'Dealer/Visa/visaList', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('58', '57', '订单列表', 'Dealer/Orders/orderList', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('59', '0', '财务管理', 'Dealer/Finance', 'building', '4', '1');
INSERT INTO `oto_supplier_nav` VALUES ('60', '59', '财务结算', 'Dealer/Finance/financeList', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('62', '59', '营业收入报表', 'Dealer/Finance/report', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('63', '0', '商家管理', 'Dealer/Shops', 'server', '3', '1');
INSERT INTO `oto_supplier_nav` VALUES ('64', '63', '添加商家', 'Dealer/Shops/addShops', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('65', '63', '交通供应商', 'Dealer/Shops/trafficShops', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('66', '63', '酒店供应商', 'Dealer/Shops/hotelShops', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('67', '63', '景区供应商', 'Dealer/Shops/scenicShops', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('68', '63', '保险供应商', 'Dealer/Shops/insureShops', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('69', '65', '合作记录', 'Dealer/Shops/trafficPartnerRecord', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('70', '65', '商家产品列表', 'Dealer/Shops/trafficProductById', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('71', '65', '线路审核', 'Dealer/Shops/trafficAuditById', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('72', '65', '线路上下架', 'Dealer/Shops/trafficStatusById', '', null, '1');
INSERT INTO `oto_supplier_nav` VALUES ('78', '39', '发布线路', 'Dealer/Line/add', '', null, '1');
