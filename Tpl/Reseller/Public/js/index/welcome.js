$('#checkAll').on('click', function(){
    if($(this).prop('checked')){
        $('.input-checkbox').prop('checked', true);
    }else{
        $('.input-checkbox').prop('checked', false);
    }
})
$('#dateChange').on('change', function(){
    if($(this).val() == 1){
       $('#selectYear').show();
       $('#barDate').hide();
    }else{
        $('#selectYear').hide();
       $('#barDate').show();
    }
})


dateFormat($('#barDate'), 'YYYY-MM');
dateFormat($('#circDate'), 'YYYY-MM-DD');

/**
 * 绑定日期选择器
 * @param  {[obj]}    obj        [元素]
 * @param  {[string]} dateFormat [时间格式]
 */
function dateFormat(obj, dateFormat){
    jeDate({
        dateCell: '#'+$(obj).attr('id'),
        format: dateFormat,
        isinitVal:false,
        isTime:true, //isClear:false,
        okfun:function(val){
        }
    });
}

/**
 * 打开订单详情页面
 * @param  {[int]}    orderId     [订单id]
 */
function openDetail(oId){
    layer.open({
        type: 2,
        area: ['100%','100%'],
        title: '订单详情',
        content: '/Reseller/Index/orderDetail/order_id/'+oId,
    })
}


/**
 * 直接用其构造函数既可创建图表
 * 条形图
 **/
 function createBarChart(title, array, orderNum, profit, endNeedPay){
    // console.log(array);
    // console.log(orderNum);
    // console.log(profit);
    // console.log(endNeedPay);
    var chart = new Highcharts.Chart('barChart', {
        title: {
            text: title,
            x: -20
        },
        xAxis: {
            // ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
            categories: array
        },
        yAxis: {
            title: {
                text: '营业额'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        // tooltip: {
        //     valueSuffix: '°C'
        // },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '订单数量',
            // data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.9, 30.7, 33.3, 44.4, 55.5]
            data: orderNum
        }, {
            name: '毛利',
            // data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8,  25.2, 26.9, 30.7, 33.3, 44.4]
            data: profit
        }, {
            name: '营业额',
            // data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 22.0, 24.8,  25.2, 26.9, 30.7]
            data: endNeedPay
        }]
    });
 }


function createPieChart($obj, title, conf){
    $obj.highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: title
        },
        tooltip: {
            headerFormat: '{series.name}<br>',
            pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: ' ',
            data: conf
        }]
    })
}