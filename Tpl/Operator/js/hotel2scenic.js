
	//国点击事件
	$(".level1").on('change',function(){
		$selval = $("select[name='level1']").val();
		$("select[name='level2']").remove();
		$.ajax({
                url:$dependency,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(arr){
                	console.info(arr);
                	//组装select增加到后面
                	var op = '<select class="level2" name="level2">';
                		op += '<option value="-1">请选择省份</option>';
                		for(var i in arr['data']){

                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
                		}
                		op += '</select>'; 
                	$("#ssd").append(op);
                	
                }
        });        
	});
	//省点击事件
	$("body").on('change','.level2',function(){
						$selval = $("select[name='level2']").val();
						//清空他的下级
						$("select[name='level3']").remove();
						$("select[name='level4']").remove();
						$.ajax({
				                url:$dependency,
				                dataType:'json',
				                type:'post',
				                cache:false,
				                data:{id:$selval},
				                success:function(arr){
				                	console.info(arr);
				                	//组装select增加到后面
				                	var op = '<select class="level3" name="level3">';
				                	op += '<option value="-1">请选择城市</option>';
				                		for(var i in arr['data']){

				                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
				                		}
				                		op += '</select>'; 
				                	$("#ssd").append(op);
				                	

				                }
				        });        
	});
	//市点击事件
	$("body").on('change','.level3',function(){
										$selval = $("select[name='level3']").val();
										//城市级别，显示选择按钮
										$(".selht").show();
										//移除下级
										$("select[name='level4']").remove();

										$.ajax({
								                url:$dependency,
								                dataType:'json',
								                type:'post',
								                cache:false,
								                data:{id:$selval},
								                success:function(arr){
								                	console.info(arr);
								                	//组装select增加到后面
								                	var op = '<select class="level4" name="level4">';
								                		op += '<option value="-1">请选择地区</option>';
								                		for(var i in arr['data']){

								                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
								                		}
								                		op += '</select>'; 
								                	$("#ssd").append(op);
								                }
								        });        
	});



	//选择酒店点击事件
	$("#selht").on('click',function(){

		//获取当前录入的国-省-市-区
		var guo = $("select[name='level1']").val()?$("select[name='level1']").val():0;
		var sheng = $("select[name='level2']").val()?$("select[name='level2']").val():0;
		var shi  = $("select[name='level3']").val()?$("select[name='level3']").val():0;
		var dq  = $("select[name='level4']").val()?$("select[name='level4']").val():0;
		$.ajax({
                url:$showmsg,
                dataType:'json',
                type:'post',
                cache:false,
                data:{guo:guo,sheng:sheng,shi:shi,dq:dq,type:'hotel'},
                success:function(data){
                	console.info(data);
                	//组装数据
                	$("#tr_hotel").empty();
                		var tr = '';
                		for(var i in data['hotels']){
                			tr += '<tr>';
                			tr += '<td style="text-align: center;"><span>'+data['hotels'][i]['supplier_name']+'</th>';
                			tr += '<td style="text-align: center;">';
                			for(var j in data['hotels'][i]['rooms']){
                				if(j < 1){
                					tr += '<input name="room_priceId" type="radio" checked value="'+data['hotels'][i]['rooms'][j]['room_priceId']+'"/><span id="rooms-'+data['hotels'][i]['rooms'][j]['room_priceId']+'">'+data['hotels'][i]['rooms'][j]['room_name']+'</span>';

                				}else{
                					tr += '<input name="room_priceId" type="radio" value="'+data['hotels'][i]['rooms'][j]['room_priceId']+'"/><span id="rooms-'+data['hotels'][i]['rooms'][j]['room_priceId']+'">'+data['hotels'][i]['rooms'][j]['room_name']+'</span>';
                				}	
                			}
                			
                			tr += '</td> ';
                			tr += '</tr>';
                		}
                	$("#tr_hotel").append(tr);		
                	$('#bjy-add').modal('show');
                	


                }
        });


	});
//添加酒店房间点击事件

$("body").on('click','.add_hotel',function(){
      	
						//酒店资料
						$room_id = $("input[name='room_priceId']:checked").val();
						$hotel_name = $("input[name='room_priceId']:checked").parent('td').prev('td').find('span').text();
						$room_name = $("#rooms-"+$room_id).text();
						console.log($room_id);
						if($room_id > 0 && $room_id != undefined){
							var tr = '<tr style="line-height: 12px;">';
							tr += '<input name="room_priceIds[]" type="hidden" value="'+$room_id+'" />';
							tr += '<td class="mytd">'+$hotel_name+'</td>';
							tr += '<td class="mytd">'+$room_name+'</td>';
							tr += '<td class="mytd"><input type="button" class="btn btn-danger deltr" value="删除"/></td>';
							tr += '</tr>';
							//组装数据生成li展示到页面上
							$("#hotels_list").append(tr);

							$('#bjy-add').modal('hide');

						}else{

							layer.msg('必须选中一个酒店-房间！');return false;
						}
});

$('body').on('click','.deltr',function(){

	$(this).parent().parent('tr').remove();

});
// 景区

	//国点击事件
	$(".levels1").on('change',function(){
		$selval = $("select[name='levels1']").val();
		$("select[name='levels2']").remove();
		$.ajax({
                url:$dependency,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(arr){
                	console.info(arr);
                	//组装select增加到后面
                	var op = '<select class="levels2" name="levels2">';
                		op += '<option value="-1">请选择省份</option>';
                		for(var i in arr['data']){

                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
                		}
                		op += '</select>'; 
                	$("#ssb").append(op);
                	
                }
        });        
	});
	//省点击事件
	$("body").on('change','.levels2',function(){
						$selval = $("select[name='levels2']").val();
						//清空他的下级
						$("select[name='levels3']").remove();
						$("select[name='levels4']").remove();
						$.ajax({
				                url:$dependency,
				                dataType:'json',
				                type:'post',
				                cache:false,
				                data:{id:$selval},
				                success:function(arr){
				                	console.info(arr);
				                	//组装select增加到后面
				                	var op = '<select class="levels3" name="levels3">';
				                	op += '<option value="-1">请选择城市</option>';
				                		for(var i in arr['data']){

				                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
				                		}
				                		op += '</select>'; 
				                	$("#ssb").append(op);
				                	

				                }
				        });        
	});
	//市点击事件
	$("body").on('change','.levels3',function(){
		$selval = $("select[name='levels3']").val();
		//城市级别，显示选择按钮
		$(".seljq").show();
		//移除下级
		$("select[name='levels4']").remove();

		$.ajax({
                url:$dependency,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(arr){
                	console.info(arr);
                	//组装select增加到后面
                	var op = '<select class="levels4" name="levels4">';
                		op += '<option value="-1">请选择地区</option>';
                		for(var i in arr['data']){

                			op += '<option value="'+arr['data'][i]['areaId']+'">'+arr['data'][i]['areaName']+'</option>';
                		}
                		op += '</select>'; 
                	$("#ssb").append(op);
                }
        });        
	});



	//选择景区点击事件
	$("body").on('click','#seljq',function(){

		//获取当前录入的国-省-市-区
		var guo = $("select[name='levels1']").val()?$("select[name='levels1']").val():0;
		var sheng = $("select[name='levels2']").val()?$("select[name='levels2']").val():0;
		var shi  = $("select[name='levels3']").val()?$("select[name='levels3']").val():0;
		var  dq  = $("select[name='levels4']").val()?$("select[name='levels4']").val():0;
		$.ajax({
                url:$showmsg,
                dataType:'json',
                type:'post',
                cache:false,
                data:{guo:guo,sheng:sheng,shi:shi,dq:dq,type:'scenic'},
                success:function(data){
                	console.info(data);
                	//组装数据
                	$("#tr_scenic").empty();
                		var tr = '';
                		for(var i in data['scenics']){
                			tr += '<tr>';
                			tr += '<td style="text-align: center;"><span>'+data['scenics'][i]['supplier_name']+'</th>';
                			tr += '<td style="text-align: center;">';
                			for(var j in data['scenics'][i]['ticket']){
                				if(j < 1){
                					tr += '<input name="scenic_price_id" type="radio" checked value="'+data['scenics'][i]['ticket'][j]['scenic_price_id']+'"/><span id="scenics-'+data['scenics'][i]['ticket'][j]['scenic_price_id']+'">'+data['scenics'][i]['ticket'][j]['scenic_name']+'</span>';

                				}else{
                					tr += '<input name="scenic_price_id" type="radio"  value="'+data['scenics'][i]['ticket'][j]['scenic_price_id']+'"/><span id="scenics-'+data['scenics'][i]['ticket'][j]['scenic_price_id']+'">'+data['scenics'][i]['ticket'][j]['scenic_name']+'</span>';
                				}	
                			}
                			
                			tr += '</td> ';
                			tr += '</tr>';
                		}
                	$("#tr_scenic").append(tr);		
                	$('#scenic-add').modal('show');
                	


                }
        });


	});


//添加景区门票点击事件

$("body").on('click','.add_scenic',function(){

						//景区资料
						$ticket_id = $("input[name='scenic_price_id']:checked").val();
						$scenic_name = $("input[name='scenic_price_id']:checked").parent('td').prev('td').find('span').text();
						$ticket_name = $("#scenics-"+$ticket_id).text();

						if($ticket_id > 0 && $ticket_id != undefined){

							var tr = '<tr style="line-height: 12px;">';
							tr += '<input name="scenic_price_ids[]" type="hidden" value="'+$ticket_id+'" />';
							tr += '<td class="mytd">'+$scenic_name+'</td>';
							tr += '<td class="mytd">'+$ticket_name+'</td>';
							tr += '<td class="mytd"><input type="button" class="btn btn-danger deltr" value="删除"/></td>';
							tr += '</tr>';
							//组装数据生成li展示到页面上
							$("#scenics_list").append(tr);

						}else{

							layer.msg('必须选中一个景区-门票！');return false;
						}

						$('#scenic-add').modal('hide');
});

// 新增保险项目
$("select[name='add_insure']").on('change',function(){

	var val = $("select[name='add_insure']").val();

	$.ajax({
		url:$find_insure,
		dataType:'json',
		type:'post',
		cache:false,
		data:{insure_id:val},
		success:function(data){
			console.info(data);
			if(data){
				//生成对应的tr
				var tr = '';
				tr += '<tr>';
				tr += '<input type="hidden" name="insure_priceId[]" value="'+data['insure_priceId']+'"/>';
				tr += '<td class="mytd">'+data['name']+'</td>';
				tr += '<td class="mytd">'+data['money_type']+'</td>';
				tr += '<td class="mytd"><input type="button" class="btn btn-danger deltr" value="删除"/></td>';
				tr += '</tr>';
				$("#insures_list").append(tr);
			}
		}
	});


});

$('body').on('click','.deltr',function(){

	$(this).parent().parent('tr').remove();

});

$("input[type='radio']").click(function(){
	console.log($(this).val());

	if($(this).val() < 0){
		
		$("#chutuan").hide();
		$("#chutuan2").show();
		layer.open({
	      type: 2,
	      title: '设置出团的时间',
	      maxmin: true,
	      shadeClose: true, //点击遮罩关闭层
	      area : ['80%' , '50%'],
	      content: 'canader2.html',
	      fix:false,
	    });
	}else{
		$("#chutuan").show();
		$("#wicth_days").val('');
	}


});
