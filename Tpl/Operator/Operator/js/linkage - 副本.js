$(document).ready(function(){
	$(".start").bind('change',function(){
		var mold = 'go';
		search(mold);
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}

		//根据当前id查询下级属性的json数组集合
		$('.start_level2').remove();
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
              
                	var select =   '<select name="start_level2" class="start_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#start").append(select);

						$("select[name='start_level2']").on('change',function(ids){
							search(mold);
							//清除同级的城市
							$(".start_level3").remove();
                			$selval = $(this)[0].value;
                			$.ajax({
					                url:$linkage,
					                dataType:'json',
					                type:'post',
					                cache:false,
					                data:{id:$selval},
					                success:function(ids){
					                	
					                	if(ids != null && ids != undefined && ids != ''){

					                		var select =   '<select name="start_level3" class="start_level3" >';
					                        select +=  '<option value="-1">请选择城市</option>';
					                        for(var vo in ids){
					                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
					                        }

											select +=  '</select>';
											$("#start").append(select);

											$("select[name='start_level3']").on('change',function(ids){
												search(mold);
												//清除同级的地区
												$(".start_level4").remove();
					                			$selval = $(this)[0].value;
					                			$.ajax({
										                url:$linkage,
										                dataType:'json',
										                type:'post',
										                cache:false,
										                data:{id:$selval},
										                success:function(ids){
										       
										                	if(ids != null && ids != undefined && ids != ''){

										                		var select =   '<select name="start_level4" class="start_level4" >';
										                        select +=  '<option value="-1">请选择地区</option>';
										                        for(var vo in ids){
										                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
										                        }

																select +=  '</select>';
																$("#start").append(select);
																$("select[name='start_level4']").on('change',function(ids){
																	search(mold);
																});	

										                	}

										                		
										                }

										        });

					                		});



						                	}

						                		
						                }

						        });

                		});

                		
                		
                }

        });
	});


	$(".end").bind('change',function(){
		var mold = 'go';
		search(mold);
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}
		//根据当前id查询下级属性的json数组集合
		$('.end_level2').remove();
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
                
                	var select =   '<select name="end_level2" class="end_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#end").append(select);

						$("select[name='end_level2']").on('change',function(ids){
							search();
							//清除同级的城市
							$(".end_level3").remove();
                			$selval = $(this)[0].value;
                			$.ajax({
					                url:$linkage,
					                dataType:'json',
					                type:'post',
					                cache:false,
					                data:{id:$selval},
					                success:function(ids){
					                
					                	if(ids != null && ids != undefined && ids != ''){
					                	var select =   '<select name="end_level3" class="end_level3" >';
					                        select +=  '<option value="-1">请选择城市</option>';
					                        for(var vo in ids){
					                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
					                        }

											select +=  '</select>';
											$("#end").append(select);

											$("select[name='end_level3']").on('change',function(ids){
												search(mold);
												//清除同级的城市
												$(".end_level4").remove();
					                			$selval = $(this)[0].value;
					                			$.ajax({
										                url:$linkage,
										                dataType:'json',
										                type:'post',
										                cache:false,
										                data:{id:$selval},
										                success:function(ids){
										                
										                	if(ids != null && ids != undefined && ids != ''){
										                	var select =   '<select name="end_level4" class="end_level4" >';
										                        select +=  '<option value="-1">请选择城市</option>';
										                        for(var vo in ids){
										                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
										                        }

																select +=  '</select>';
																$("#end").append(select);
																$("select[name='end_level4']").on('change',function(ids){
																	search(mold);
																});	
															}	
										                		
										                }

										        });

					                		});

										}	
					                		
					                }

					        });

                		});

                		
                		
                }

        });
	});


	//回目的地
	$(".back_start").bind('change',function(){
		var mold = 'back';
		search(mold);
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}
		//根据当前id查询下级属性的json数组集合
		$('.back_start_level2').remove();
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
                	console.info(ids);
                	var select =   '<select name="back_start_level2" class="back_start_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#back_start").append(select);

						$("select[name='back_start_level2']").on('change',function(ids){
							search();
							//清除同级的城市
							$(".back_start_level3").remove();
                			$selval = $(this)[0].value;
                			$.ajax({
					                url:$linkage,
					                dataType:'json',
					                type:'post',
					                cache:false,
					                data:{id:$selval},
					                success:function(ids){
					                	
					                	if(ids != null && ids != undefined && ids != ''){

					                		var select =   '<select name="back_start_level3" class="back_start_level3" >';
					                        select +=  '<option value="-1">请选择城市</option>';
					                        for(var vo in ids){
					                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
					                        }

											select +=  '</select>';
											$("#back_start").append(select);

											$("select[name='back_start_level3']").on('change',function(ids){
												search(mold);
												//清除同级的地区
												$(".back_start_level4").remove();
					                			$selval = $(this)[0].value;
					                			$.ajax({
										                url:$linkage,
										                dataType:'json',
										                type:'post',
										                cache:false,
										                data:{id:$selval},
										                success:function(ids){
										                
										                	if(ids != null && ids != undefined && ids != ''){

										                		var select =   '<select name="back_start_level4" class="start_level4" >';
										                        select +=  '<option value="-1">请选择地区</option>';
										                        for(var vo in ids){
										                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
										                        }

																select +=  '</select>';
																$("#back_start").append(select);

										                		$("select[name='back_start_level4']").on('change',function(ids){
																	search(mold);
																});	

										                	}


										                		
										                }

										        });

					                		});



						                	}

						                		
						                }

						        });

                		});

                		
                		
                }

        });
	});


	$(".back_end").bind('change',function(){
		var mold = 'back';
		search(mold);
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}
		//根据当前id查询下级属性的json数组集合
		$('.back_end_level2').remove();
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
         
                	var select =   '<select name="back_end_level2" class="back_end_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#back_end").append(select);

						$("select[name='back_end_level2']").on('change',function(ids){
							search(mold);
							//清除同级的城市
							$(".back_end_level3").remove();
                			$selval = $(this)[0].value;
                			$.ajax({
					                url:$linkage,
					                dataType:'json',
					                type:'post',
					                cache:false,
					                data:{id:$selval},
					                success:function(ids){
					         
					                	if(ids != null && ids != undefined && ids != ''){
					                	var select =   '<select name="back_end_level3" class="back_end_level3" >';
					                        select +=  '<option value="-1">请选择城市</option>';
					                        for(var vo in ids){
					                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
					                        }

											select +=  '</select>';
											$("#end").append(select);

											$("select[name='back_end_level3']").on('change',function(ids){
												search(mold);
												//清除同级的城市
												$(".back_end_level4").remove();
					                			$selval = $(this)[0].value;
					                			$.ajax({
										                url:$linkage,
										                dataType:'json',
										                type:'post',
										                cache:false,
										                data:{id:$selval},
										                success:function(ids){
										         
										                	if(ids != null && ids != undefined && ids != ''){
										                	var select =   '<select name="back_end_level4" class="end_level4" >';
										                        select +=  '<option value="-1">请选择城市</option>';
										                        for(var vo in ids){
										                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
										                        }

																select +=  '</select>';
																$("#back_end").append(select);
																$("select[name='back_end_level4']").on('change',function(ids){
																	search(mold);
																});	

															}	
										                		
										                }

										        });

					                		});

										}	
					                		
					                }

					        });

                		});

                		
                		
                }

        });
	});

	//固定函数，每次选择下拉点击事件就获取表单相关字段传递给控制器处理获取对应的数据
    function search(mold,kind){
    	console.info(mold);
    	//如果参数mold为go就是去，back就是回
    	$(".traffics-"+mold).show();
    	$(".suppliers-"+mold).show();
    	$(".seats-"+mold).show();
    	if(mold == 'go'){
    		var go_guo = $("select[name='start_level1']").val() > 0?$("select[name='start_level1']").val():0;
		    var go_sheng = $("select[name='start_level2']").val()>0?$("select[name='start_level2']").val():0;
		    var go_shi = $("select[name='start_level3']").val()>0?$("select[name='start_level3']").val():0;
		    var go_dq =  $("select[name='start_level4']").val()>0?$("select[name='start_level4']").val():0;
		    var bk_guo = $("select[name='end_level1']").val()>0?$("select[name='end_level1']").val():0;
		    var bk_sheng = $("select[name='end_level2']").val()>0?$("select[name='end_level2']").val():0;
		    var bk_shi = $("select[name='end_level3']").val()>0?$("select[name='end_level3']").val():0;
		    var bk_dq  = $("select[name='end_level4']").val()>0?$("select[name='end_level4']").val():0;

    	}else{
    		var go_guo = $("select[name='back_start_level1']").val() > 0?$("select[name='back_start_level1']").val():0;
		    var go_sheng = $("select[name='back_start_level2']").val()>0?$("select[name='back_start_level2']").val():0;
		    var go_shi = $("select[name='back_start_level3']").val()>0?$("select[name='back_start_level3']").val():0;
		    var go_dq =  $("select[name='back_start_level4']").val()>0?$("select[name='back_start_level4']").val():0;
		    var bk_guo = $("select[name='back_end_level1']").val()>0?$("select[name='back_end_level1']").val():0;
		    var bk_sheng = $("select[name='back_end_level2']").val()>0?$("select[name='back_end_level2']").val():0;
		    var bk_shi = $("select[name='back_end_level3']").val()>0?$("select[name='back_end_level3']").val():0;
		    var bk_dq  = $("select[name='back_end_level4']").val()>0?$("select[name='back_end_level4']").val():0;

    	}

    	var go_tf_id = $("select[name='go_traffic_id']").val()>0?$("select[name='go_traffic_id']").val():0; 
    	var bk_tf_id = $("select[name='back_traffic_id']").val()>0?$("select[name='back_traffic_id']").val():0; 
    	var go_sp_id = $("select[name='go_supplier_id']").val()>0?$("select[name='go_supplier_id']").val():0;
    	var bk_sp_id = $("select[name='back_supplier_id']").val()>0?$("select[name='back_supplier_id']").val():0;

	   	$.ajax({
            url:$search,
            dataType:"json",
            type:'POST',
            cache:false,
            data:{go_guo:go_guo,go_sheng:go_sheng,go_shi:go_shi,go_dq:go_dq,bk_guo:bk_guo,bk_sheng,bk_sheng,bk_shi:bk_shi,bk_dq:bk_dq,go_tf_id:go_tf_id,bk_tf_id:bk_tf_id,go_sp_id:go_sp_id,bk_sp_id:bk_sp_id},
            success: function(rs) {
            	// $("traffic-add").modal('show');	
            	if(kind == 'traffics'){

            		$("option[class='suppliers_op']").remove();	
            	}else if(kind == 'suppliers'){
					$("option[class='seats_op']").remove();
            	}else{
            		$("option[class='traffics_op']").remove();
            	}

     //        	if(kind == 'seats' || kind == '' || kind == undefined){
					// $("option[class='traffics_op']").remove();	
     //        	}


            		
  

            	
            	
            	if(rs != '' && rs != null && rs != undefined){

	            	console.log(rs);
	            	//data供应商
	            	data = rs['suppliers'];
	            	var op = '';
	            	for(var vo in data){
	  
	            		op += '<option class="suppliers_op" value="'+data[vo]['supplier_id']+'">'+data[vo]['supplier_name']+'</option>';

	            	}

	            	//data2交通方式
	            	data2 = rs['traffics'];
	            	var op2 = '';
	            	for(var v in data2){
	  
	            		op2 += '<option class="traffics_op" value="'+data2[v]['traffic_id']+'">'+data2[v]['traffic_name']+'</option>';

	            	}

	            	data3 = rs['seats'];
	            	var op3 = '';
	            	for(var t in data3){
	  
	            		op3 += '<option class="seats_op" value="'+data3[t]['seat_id']+'">'+data3[t]['seat_name']+'</option>';

	            	}


	            	if(kind == 'traffics'){
	            		$(".suppliers_"+mold).append(op);
	            		// $(".seats_"+mold).append(op3);
	            	}else if(kind == 'suppliers'){
	 
	            		// $(".traffics_"+mold).append(op2);
	            		$(".seats_"+mold).append(op3);
	            	}else{
	            		$(".traffics_"+mold).append(op2);
	            		// $(".suppliers_"+mold).append(op);
	            		// $(".seats_"+mold).append(op3);
	            	}

	
	            		
	     
	            	

	            	$("#traffics_"+mold).on('change',function(){
	            		search(mold,'traffics');
	            	});


	            	$("#suppliers_"+mold).on('change',function(){
	            		search(mold,'suppliers');
	            	});

	            	// $("#seats_"+mold).on('change',function(){
	            	// 	search(mold,'seats');
	            	// });
	   

	       // console.info(rs);return false;



            	}
            	// else{
            	// 	layer.msg('没有供应商！');return false;
            	// }
            }
    	});
    }	




	

});