<?php
namespace Operator\Model;
/**
 * 游客(会员)资料模型
 */
class VisitorModel extends BaseModel{

    protected $tableName = 'public_visitor_info';

    /**
     * 生成新的会员卡号
     */
    public  function cardNum(){
        $date = date('YmdHis',time());
        $card_num = $date.time().mt_rand(100000,999999);
        $isHave =$this->where(array('card_num'=>$card_num))->getField('card_num');
        if (!$isHave) {
        	return $card_num;
        }
        return $card_num+mt_rand(100,999);
    }

 	/**
     * 会员列表数据
     */
    public function getAllVisitorInfo($where){
    	// 保存筛选条件
    	session('VISITOR_WHERE', $where);
        $count      = $this->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=$this->where($where)->order('entry_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        return ['list'=>$list,'show'=>$show];
    }
 	/**
     * 添加会员
     */
    public function addVisitor($data){
    	$admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $data['operator_id']=$admin_id;
        $data['entry_time'] = time();
        unset($data['visitor_id']);
        if($this->create($data)){
            return $this->add();
        }
        return false;
    }
    /**
     * 修改会员信息
     */
    public function editVisitor($data){
    		// 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
        	$map['visitor_id']=$data['visitor_id'];
            unset($data['visitor_id']);
            $result=$this
                ->where(array($map))
                ->save($data);
            return $result;
        }

    }
    /**
     * 根据ID 获取会员资料
     */
    public function getVisitorInfoById($id){
    	$visitorInfo=$this->where(array('visitor_id'=>$id))->select();
    	return $visitorInfo[0];
    }

    /**
     * 开通与锁定会员状态
     */
    public function changeVisitorStatusById($data){
        return $this->where(['visitor_id'=>$data['uid']])->setField(['visitor_status'=>$data['status']]);
    }


    /**
     * Excel导出会员信息列表-根据页面当前的筛选条件筛选
     */
    public function outVisitorExportExcel(){
    	$where = session('VISITOR_WHERE'); 
    	$list=$this->where($where)->order('entry_time desc')->select();
    	foreach ($list as $k=>$v){
            $list[$k]['source_type']=$v['source_type']==1?'直营门店':'分销商';
            $list[$k]['visitor_status']=$v['visitor_status']==1?'开通':'锁定';
            $list[$k]['visitor_sex']=$v['status']==1?'男':'女';
            $list[$k]['entry_time']=date('Y-m-d',$v['entry_time']);
        }
        $expCellName  = array(
            array('visitor_name','姓名'),
            array('visitor_sex','性别'),
            array('documents_num','证件号'),
            array('visitor_phone','电话'),
            array('card_num','卡号'),
            array('source_type','来源属性'),
            array('total_consumption','消费总额'),
            array('total_grade','累计积分'),
            array('consume_num','消费次数'),
            array('entry_time','创建时间'),
        );

        $fileName='会员资料表';
        parent::exportExcel($fileName,$expCellName,$list);
    }

    /**
     * 获取会员等级列表
     */
    public function getGradeConfing(){
        $operator_id = session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $gradeInfo = M('public_grade_configs')->where(array('operator_id'=>$operator_id))->select();
        return $gradeInfo;
    }
    /**
     *  批量保存会员等级设置
     */
    public function saveGrade(){
        // dump(I('newNo'));
        $rd = array('status'=>-1);
        $operator_id = session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $m = M('public_grade_configs');
        // 保存新的
        $newNo = (int)I('newNo');
        for($i=0;$i<$newNo;$i++){
            $data=array();
            $data['grade_score'] =I('grade_score_n_'.$i);
            $data['grade_name'] =I('grade_name_n_'.$i);
            $data['operator_id'] = $operator_id;
            // dump($data);
            // 查重
            $isHave1 = $m->where(array('grade_score'=>$data['grade_score']))
                         ->getField('grade_id');
            $isHave2 = $m->where(array('grade_name'=>$data['grade_name']))
                         ->getField('grade_id');
            if ($isHave1 || $isHave2) {
                continue;
            }else{
                $m->add($data);
            }

        }

        // 更新旧的
        $oldNo = (int)I('oldNo');
        for($i=0;$i<$oldNo;$i++){
            $data=array();
            $map['grade_id'] = I('grade_id_o_'.$i);
            $data['grade_score'] =I('grade_score_o_'.$i);
            $data['grade_name'] =I('grade_name_o_'.$i);
            // 查重
            $isHave1 = $m->where(array('grade_score'=>$data['grade_score'],'grade_id'=>array('neq',$map['grade_id'])))
                         ->getField('grade_id');
            $isHave2 = $m->where(array('grade_name'=>$data['grade_name'],'grade_id'=>array('neq',$map['grade_id'])))
                         ->getField('grade_id');
            if ($isHave1 || $isHave2) {
                continue;
            }else{
                $m->where(array($map))->save($data);
            }
        }

        $rd['status'] = 1;
        return $rd;

    }
}

