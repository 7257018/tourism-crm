<?php
namespace Operator\Model;
/**
 * 积分模型
 */
class ScoreModel extends BaseModel{

    // protected $tableName = 'public_visitor_info';


    /**
     * 获取积分设置
     */
    public function getScoreCofing(){
        $operator_id = session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $scoreInfo = M('public_score_configs')->where(array('operator_id'=>$operator_id))->find();
        return $scoreInfo;
    }

    /**
     * 修改积分设置
     */
    public function editScoreCofing($data){
        // 对data数据进行验证
        $operator_id = session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $data['operator_id']=$operator_id;

        if(!$data=M('public_score_configs')->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            if(self::getScoreCofing()){
                // 验证通过
                $map['id']=$data['id'];
                unset($data['id']);
                $result=M('public_score_configs')
                    ->where(array($map))
                    ->save($data);
            }else{
                unset($data['id']);
                $result=M('public_score_configs') ->add($data);
            }
            return $result;
        }

    }
}

