<?php
namespace Operator\Model;
/**
 * ModelName
 */
class VisaModel extends BaseModel{

    protected $tableName = 'public_visa';

    /**
     * 添加签约
     */
    public function  addVisa($data){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $addDate['visa_name']=$data['visa_name'];
        $addDate['visa_price']=$data['visa_price'];
        $addDate['sample']=$data['sample'];
        $addDate['operator_id']=$admin_id;
        return $this->data($addDate)->add();
    }

    /**
     * 签约列表
     */
    public  function  getList(){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $count      = $this->where(['supplier_id'=>$admin_id,'is_delete'=>1])->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=$this->where(['operator_id'=>$admin_id,'is_delete'=>1])->order('visa_id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        return ['list'=>$list,'show'=>$show];

    }


    /**
     * 签证详情
     */
    public  function  getDetail($visa_id){
        return $this->find($visa_id);
    }


    /**
     * 更改上下架状态
     */
    public  function  visaStatus($visa_id,$visa_status){
        $res=$this->where(['visa_id'=>$visa_id])->setField(['visa_status'=>$visa_status]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }


    /**
     * 更新签约
     */
    public function  editVisa($data){
        $res=$this->data($data)->save();
        return $res;
    }
}

