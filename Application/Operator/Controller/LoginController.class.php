<?php
namespace Operator\Controller;
use Think\Controller;
/**
 * 后台首页控制器
 */
class LoginController extends Controller{



    /**
     * 登录
     */
    public function login(){

        $isLoginOut=I('logout',0,'intval');
        if($isLoginOut){
            session('operator_user',null);
            $this->success('退出成功!','/Operator/Login/login',array('logout'=>0));
            exit();
        }


        if(IS_AJAX){
            $userName=I('username');
            $password=I('password');
            $code=I('code');
            $verify = new \Think\Verify();
            // if(!$verify->check($code)){
            //     $this->ajaxReturn(['status'=>-1,'msg'=>'验证码错误!']);
            // }

            $userInfo=M('operator')->where(array('operator_phone'=>$userName,'operator_status'=>1,'operator_flag'=>1))->find();
            if($userInfo['login_pwd']==md5($password.$userInfo['login_secret'])){
                session('operator_user',$userInfo);
                //如果是超管,拥有所有权限
                session('IS_ADMIN',$userInfo['pid']==0?1:0);
                $this->ajaxReturn(['status'=>1,'msg'=>'登录成功!']);
            }
	    $this->ajaxReturn(['status'=>0,'msg'=>'登录失败']);

        }
        $this->display();
    }



    /**
     * 显示验证码
     */
    public  function  showCode(){
        ob_clean();
        validateCode();
    }




}
