<?php
return array(
    'AUTH_CONFIG'=>array(
        'AUTH_ON'           => true, // 认证开关
        'AUTH_TYPE'         => 1, // 认证方式，1为实时认证；2为登录认证。
        'AUTH_GROUP'        => C('DB_PREFIX').'operator_auth_group', // 用户组数据表名
        'AUTH_GROUP_ACCESS' => C('DB_PREFIX').'operator_auth_group_access', // 用户-用户组关系表
        'AUTH_RULE'         => C('DB_PREFIX').'operator_auth_rule', // 权限规则表
        'AUTH_USER'         => C('DB_PREFIX').'operator', // 用户信息表
    ),
    //不用验证的控制器
    'NOT_VALIDATE'=>array(
        'Operator/Login/login',
        'Operator/Platform/getCity',
        'Operator/Platform/getProvince',
        'Operator/Platform/getArea',
        'Operator/Base/oplog',//日志
        'Operator/Index/welcome',//欢迎页
        'Operator/Login/showCode',//显示验证码
        'Operator/Closing/supplierClosing',//下单时计算供应商成本
        //发布线路
        'Operator/Line/destinationCity',//发布线路目的城市选择
        'Operator/Line/choseHotel',//选择酒店
        'Operator/Line/upgradeHotel',//选择可升级酒店
        'Operator/Line/choseScenic',//选择景区
        'Operator/Line/choseInsure',//选择保险
        'Operator/Line/canader3',//单独日历选择出团日期
        'Operator/Line/choseGoTraffic',//根据出发城市及目的城市,返回可选的交通方式
        'Operator/Line/upgradeTrafficList',//可升级交通
        'Operator/Line/step1_action',//发布线路第一步入库
        'Operator/Line/form_step2',//发布线路第二步
        'Operator/Line/step2_action',//发布线路第二步入库
        'Operator/Line/specialDayPrice',//特殊日期价格
        'Operator/Line/form_step3',//发布线路第三步
        'Operator/Line/step3_action',//发布线路第三步入库
        'Operator/Line/form_step4',//发布线路第四步
        'Operator/Line/visa_ajax',//ajax获取签证信息
        'Operator/Line/step4_action',//第四步入库
        'Operator/Line/form_step5',//发布线路第5步
        'Operator/Line/step5_action',//发布线路第5步入库
        'Operator/Line/form_step6',//发布线路第6步
        'Operator/Line/step6_action',//发布线路第6步入库
        'Operator/Base/getCurrency',//获取当前币种
        //编辑线路
        'Operator/EditLine/destinationCity',//发布线路目的城市选择
        'Operator/EditLine/choseHotel',//选择酒店
        'Operator/EditLine/upgradeHotel',//选择可升级酒店
        'Operator/EditLine/choseScenic',//选择景区
        'Operator/EditLine/choseInsure',//选择保险
        'Operator/EditLine/canader3',//单独日历选择出团日期
        'Dealer/EditLine/choseGoTraffic',//根据出发城市及目的城市,返回可选的交通方式
        'Operator/EditLine/upgradeTrafficList',//可升级交通
        'Operator/EditLine/step1_action',//发布线路第一步入库
        'Operator/EditLine/form_step2',//发布线路第二步
        'Operator/EditLine/step2_action',//发布线路第二步入库
        'Operator/EditLine/specialDayPrice',//特殊日期价格
        'Operator/EditLine/form_step3',//发布线路第三步
        'Operator/EditLine/step3_action',//发布线路第三步入库
        'Operator/EditLine/form_step4',//发布线路第四步
        'Operator/EditLine/visa_ajax',//ajax获取签证信息
        'Operator/EditLine/step4_action',//第四步入库
        'Operator/EditLine/form_step5',//发布线路第5步
        'Operator/EditLine/step5_action',//发布线路第5步入库
        'Operator/EditLine/form_step6',//发布线路第6步
        'Operator/EditLine/step6_action',//发布线路第6步入库

        //编辑供应商提供的线路
        'Operator/SpecialLine/destinationCity',//发布线路目的城市选择
        'Operator/SpecialLine/choseHotel',//选择酒店
        'Operator/SpecialLine/upgradeHotel',//选择可升级酒店
        'Operator/SpecialLine/choseScenic',//选择景区
        'Operator/SpecialLine/choseInsure',//选择保险
        'Operator/SpecialLine/canader3',//单独日历选择出团日期
        'Dealer/SpecialLine/choseGoTraffic',//根据出发城市及目的城市,返回可选的交通方式
        'Operator/SpecialLine/upgradeTrafficList',//可升级交通
        'Operator/SpecialLine/step1_action',//发布线路第一步入库
        'Operator/SpecialLine/form_step2',//发布线路第二步
        'Operator/SpecialLine/step2_action',//发布线路第二步入库
        'Operator/SpecialLine/specialDayPrice',//特殊日期价格
        'Operator/SpecialLine/form_step3',//发布线路第三步
        'Operator/SpecialLine/step3_action',//发布线路第三步入库
        'Operator/SpecialLine/form_step4',//发布线路第四步
        'Operator/SpecialLine/visa_ajax',//ajax获取签证信息
        'Operator/SpecialLine/step4_action',//第四步入库
        'Operator/SpecialLine/form_step5',//发布线路第5步
        'Operator/SpecialLine/step5_action',//发布线路第5步入库
        'Operator/SpecialLine/form_step6',//发布线路第6步
        'Operator/SpecialLine/step6_action',//发布线路第6步入库

        //订购中心
        'Operator/OrderCenter/getLine', //获取线路
        'Operator/OrderCenter/order',   //出团时间选择页面
        'Operator/OrderCenter/getSeat', //获取当天团的余位
        'Operator/OrderCenter/checkGroup', //检测该团状态
        'Operator/OrderCenter/getCity', //获取城市列表
        'Operator/OrderCenter/tripDetail', //行程详情
        'Operator/OrderCenter/showCode',  //更新/获取验证码
        'Operator/OrderCenter/writeInfo', //step 3 写入游客信息
        'Operator/OrderCenter/submitTouristInfo', //提交订单信息
        'Operator/OrderCenter/detailView',  //订单预览
        'Operator/OrderCenter/orderChange', //修改订单实收金额
        'Operator/Index/orderDetail',       //订单详情

    ),

);