<?php
return array(
    'AUTH_CONFIG'=>array(
        'AUTH_ON'           => true, // 认证开关
        'AUTH_TYPE'         => 1, // 认证方式，1为实时认证；2为登录认证。
        'AUTH_GROUP'        =>C('DB_PREFIX'). 'dealer_auth_group', // 用户组数据表名
        'AUTH_GROUP_ACCESS' => C('DB_PREFIX').'dealer_auth_group_access', // 用户-用户组关系表
        'AUTH_RULE'         => C('DB_PREFIX').'dealer_auth_rule', // 权限规则表
        'AUTH_USER'         => C('DB_PREFIX').'operator_line_supplier', // 用户信息表
    ),
    //不用验证的控制器
    'NOT_VALIDATE'=>array(
        'Dealer/Login/login',
        'Dealer/Base/__construct',
        'Dealer/Index/index',
        'Dealer/Platform/getCity',
        'Dealer/Platform/getProvince',
        'Dealer/Platform/getCity',
        'Dealer/Platform/getArea',
        'Dealer/Platform/editPassword',
        //发布线路
        'Dealer/Line/destinationCity',//发布线路目的城市选择
        'Dealer/Line/choseHotel',//选择酒店
        'Dealer/Line/upgradeHotel',//选择可升级酒店
        'Dealer/Line/choseScenic',//选择景区
        'Dealer/Line/choseInsure',//选择保险
        'Dealer/Line/canader3',//单独日历选择出团日期
        'Dealer/Line/choseGoTraffic',//根据出发城市及目的城市,返回可选的交通方式
        'Dealer/Line/upgradeTrafficList',//可升级交通
        'Dealer/Line/step1_action',//发布线路第一步入库
        'Dealer/Line/form_step2',//发布线路第二步
        'Dealer/Line/step2_action',//发布线路第二步入库
        'Dealer/Line/specialDayPrice',//特殊日期价格
        'Dealer/Line/form_step3',//发布线路第三步
        'Dealer/Line/step3_action',//发布线路第三步入库
        'Dealer/Line/form_step4',//发布线路第四步
        'Dealer/Line/visa_ajax',//ajax获取签证信息
        'Dealer/Line/step4_action',//第四步入库
        'Dealer/Line/form_step5',//发布线路第5步
        'Dealer/Line/step5_action',//发布线路第5步入库
        'Dealer/Base/getCurrency',//获取当前币种
        //编辑线路
        'Dealer/EditLine/destinationCity',//发布线路目的城市选择
        'Dealer/EditLine/choseHotel',//选择酒店
        'Dealer/EditLine/upgradeHotel',//选择可升级酒店
        'Dealer/EditLine/choseScenic',//选择景区
        'Dealer/EditLine/choseInsure',//选择保险
        'Dealer/EditLine/canader3',//单独日历选择出团日期
        'Dealer/EditLine/choseGoTraffic',//根据出发城市及目的城市,返回可选的交通方式
        'Dealer/EditLine/upgradeTrafficList',//可升级交通
        'Dealer/EditLine/step1_action',//发布线路第一步入库
        'Dealer/EditLine/form_step2',//发布线路第二步
        'Dealer/EditLine/step2_action',//发布线路第二步入库
        'Dealer/EditLine/specialDayPrice',//特殊日期价格
        'Dealer/EditLine/form_step3',//发布线路第三步
        'Dealer/EditLine/step3_action',//发布线路第三步入库
        'Dealer/EditLine/form_step4',//发布线路第四步
        'Dealer/EditLine/visa_ajax',//ajax获取签证信息
        'Dealer/EditLine/step4_action',//第四步入库
        'Dealer/EditLine/form_step5',//发布线路第5步
        'Dealer/EditLine/step5_action',//发布线路第5步入库

        'Dealer/Index/welcome',//欢迎页
        'Dealer/Login/showCode',//显示验证码
        'Dealer/Product/applyLinePartner',//申请线路与运营商合作,后台处理方法

    ),

);