<?php
namespace Dealer\Controller;
use Think\Controller;
/**
 * 后台首页控制器
 */
class IndexController extends BaseController{

    public function __construct(){
        parent::__construct();
    }


	/**
	 * 首页
	 */
	public function index(){
		$this->display();
	}
	/**
	 * elements
	 */
	public function elements(){

		$this->display();
	}
	
	/**
	 * welcome
	 */
	public function welcome(){


	    //待审核产品列表
        $this->auditList=D('Index')->auditProductList();


        //最新消息
        $this->messList=D('Index')->messList();

        //最新订单
        $this->orderList=D('Index')->orderList();

        //营业额及毛利比例
        $toDay=I('toDay');
        $this->toDay=$toDay?$toDay:date('Y-m-d');

        //return['total'=>$total,'cost'=>$cost,'profitPercent'=>$profitPercent,'totalPercent'=>$totalPercent];

        $this->turnover=D('Index')->turnover($this->toDay);

        //可选年份
        $this->yearList=D('Index')->yearList();



        $this->report=D('Index')->report();

        $this->type=I('type',1,'intval');
        $this->year=I('year',date('Y'),'intval');
        $this->month=I('month',1,'intval');

        //p($this->report);die;

	    $this->display();
	}





}
