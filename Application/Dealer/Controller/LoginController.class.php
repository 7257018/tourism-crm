<?php
namespace Dealer\Controller;
use Think\Controller;
/**
 * 后台首页控制器
 */
class LoginController extends Controller{



    /**
     * 登录
     */
    public function  login(){

        $isLoginOut=I('logout',0,'intval');
        if($isLoginOut){
            session('dealer_user',null);
            $this->success('退出成功!','/Dealer/Login/login',array('logout'=>0));
            exit();
        }



        if(IS_AJAX){
            $userName=I('username');
            $password=I('password');
            $code=I('code');
            $verify = new \Think\Verify();
             if(!$verify->check($code)){
                 $this->ajaxReturn(['status'=>-1,'msg'=>'验证码错误!']);
             }
            $userInfo=M('operator_line_supplier')->where(array('_string'=>"supplier_account='{$userName}' or supplier_phone='{$userName}' ",'supplier_status'=>1,'supplier_flag'=>1,'is_review'=>1))->find();

            if($userInfo['loginPwd']==md5($password.$userInfo['loginSecret'])){

                //判断是否已过期
                $nowTime=time();
                if($userInfo['start_time']>$nowTime||$nowTime>$userInfo['end_time']&&$userInfo['pid']==0){
                    //线路已过期
                    M('public_line')->where(array('supplier_id'=>$userInfo['supplier_id']))->save(array('line_status'=>-4));
                    $this->ajaxReturn(['status'=>-1,'msg'=>'合作时间已过期，请联系运营商!']);
                }

                session('dealer_user',$userInfo);
                //如果是超管,拥有所有权限
                session('IS_ADMIN',$userInfo['pid']==0?1:0);
                $this->ajaxReturn(['status'=>1,'msg'=>'登录成功!']);
            }
            $this->ajaxReturn(['status'=>0,'msg'=>'帐号或者密码错误!']);

        }
        $this->display();
    }



    /**
     * 显示验证码
     */
    public  function  showCode(){
        ob_clean();
        validateCode();
    }




}
