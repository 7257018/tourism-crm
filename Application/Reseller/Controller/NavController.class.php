<?php
namespace Reseller\Controller;
/**
 * 后台菜单管理
 */
class NavController extends BaseController{

    public function __construct(){
        parent::__construct();
    }


	/**
	 * 菜单列表
	 */
	public function index(){

	    $data=D('AdminNav')->getTreeData('tree','order_number,id');
		$assign=array(
			'data'=>$data
			);

		$this->assign($assign);
		$this->display();
	}

	/**
	 * 添加菜单
	 */
	public function add(){
		$data=I('post.');
		unset($data['id']);
        $data['platform']=3;

		$result=D('AdminNav')->add($data);

		if ($result) {
			$this->success('添加成功',U('Nav/index'));
		}else{
			$this->error('添加失败');
		}
	}

	/**
	 * 修改菜单
	 */
	public function edit(){
		$data=I('post.');
        //此菜单属于供应商
        $data['platform']=3;
		$map=array(
			'id'=>$data['id']
			);
		$result=D('AdminNav')->editData($map,$data);
		if ($result) {
			$this->success('修改成功',U('Nav/index'));
		}else{
			$this->error('修改失败');
		}
	}

	/**
	 * 删除菜单
	 */
	public function delete(){
		$id=I('get.id');
		$map=array(
			'id'=>$id
			);
		$result=D('AdminNav')->delete($map);
		if($result){
			$this->success('删除成功',U('Nav/index'));
		}else{
			$this->error('请先删除子菜单');
		}
	}

	/**
	 * 菜单排序
	 */
	public function order(){
		$data=I('post.');
		$result=D('AdminNav')->orderData($data);
		if ($result) {
			$this->success('排序成功',U('Nav/index'));
		}else{
			$this->error('排序失败');
		}
	}


}
