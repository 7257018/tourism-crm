<?php
namespace Reseller\Model;
use Think\Model;

class ExportExcelModel extends Model{

    //导出excel共同方法
    public function exportExcels($expTitle,$expCellName,$expTableData){
        ob_end_clean();
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
        $fileName = $expTitle.date('_YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);

        vendor("PHPExcel.PHPExcel");

        $objPHPExcel = new \PHPExcel();
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        //合并单元格
        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');


        for($i=0;$i<$cellNum;$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'2', $expCellName[$i][1]);
        }
        // Miscellaneous glyphs, UTF-8
        for($i=0;$i<$dataNum;$i++){
            for($j=0;$j<$cellNum;$j++){
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+3), $expTableData[$i][$expCellName[$j][0]]);
                $objPHPExcel->getActiveSheet(0)->getStyle($cellName[$j].($i+3))->getAlignment()->setWrapText(true);
            }
        }

         //定制列宽
        switch ($expTitle) {
            case '会员资料表':
                $this->visitorList($objPHPExcel);
                break;
            case '发票统计表':
                $this->bill($objPHPExcel);
                break;
            case '营业收入报表':
                $this->report($objPHPExcel);
                break;
            case '订单列表':
                $this->orderList($objPHPExcel);
                break;
            case '订单回收站':
                $this->orderRecovery($objPHPExcel);
                break;
            default:
                $this->resellerList($objPHPExcel);
                break;
        }


        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$xlsTitle.'.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
    private function resellerList(&$objPHPExcel){
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('A') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('C') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('D') -> setWidth(20);
    }

    private function visitorList(&$objPHPExcel){
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('C') -> setWidth(12);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('D') -> setWidth(15);
    }

    private function bill(&$objPHPExcel){
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('A') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('B') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('I') -> setWidth(13);
    }

    private function report(&$objPHPExcel){
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('A') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('B') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('H') -> setWidth(15);
    }

    private function orderRecovery(&$objPHPExcel){
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('E') -> setWidth(12);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('F') -> setWidth(35);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('G') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('I') -> setWidth(10);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('J') -> setWidth(15);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('L') -> setWidth(15);
    }


    private function orderList(&$objPHPExcel){
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('A') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('B') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('E') -> setWidth(12);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('F') -> setWidth(35);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('G') -> setWidth(20);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('I') -> setWidth(15);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('J') -> setWidth(15);
        $objPHPExcel -> getActiveSheet(0) -> getColumnDimension('M') -> setWidth(15);
    }
}