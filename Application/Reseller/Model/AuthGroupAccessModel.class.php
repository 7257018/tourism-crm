<?php
namespace Reseller\Model;
/**
 * 权限规则model
 */
class AuthGroupAccessModel extends BaseModel{

    protected $tableName = 'reseller_auth_group_access';

    /**
	 * 根据group_id获取全部用户id
	 * @param  int $group_id 用户组id
	 * @return array         用户数组
	 */
	public function getUidsByGroupId($group_id){
		$user_ids=$this
			->where(array('group_id'=>$group_id))
			->getField('uid',true);
		return $user_ids;
	}

	/**
	 * 获取管理员权限列表
	 */
	public function getAllData(){
	    //区分哪一个平台的会员
        $userInfo=session('reseller_user');
        if($userInfo['pid']==0){
            $where=" ( u.pid={$userInfo['reseller_id']} or u.reseller_id= {$userInfo['reseller_id']} ) ";
        }else{
            $where=" ( u.pid={$userInfo['pid']} or u.reseller_id= {$userInfo['pid']} ) ";
        }
        $userName=I('userName');
        if($userName){
            $where.=" and (u.reseller_account like '%{$userName}%' or u.reseller_name like '%{$userName}%' or u.reseller_phone like '%{$userName}%')";
        }
        //u.email,
		$data=$this
			->field('u.reseller_id as id,u.reseller_name as username,aga.group_id,ag.title,u.reseller_account,u.reseller_name,u.reseller_phone,u.reseller_status,u.reseller_id')
			->alias('aga')
            ->where($where)
			->join('__OPERATOR_LINE_RESELLER__ u ON aga.uid=u.reseller_id','RIGHT')
			->join('__RESELLER_AUTH_GROUP__ ag ON aga.group_id=ag.id','LEFT')
			->select();
		// 获取第一条数据
		$first=$data[0];
		$first['title']=array();
		$user_data[$first['id']]=$first;
		// 组合数组
		foreach ($data as $k => $v) {
			foreach ($user_data as $m => $n) {
				$uids=array_map(function($a){return $a['id'];}, $user_data);
				if (!in_array($v['id'], $uids)) {
					$v['title']=array();
					$user_data[$v['id']]=$v;
				}
			}
		}
		// 组合管理员title数组
		foreach ($user_data as $k => $v) {
			foreach ($data as $m => $n) {
				if ($n['id']==$k) {
					$user_data[$k]['title'][]=$n['title'];
				}
			}
			$user_data[$k]['title']=implode('、', $user_data[$k]['title']);
		}

		// 管理组title数组用顿号连接
		return $user_data;

	}
    public function addData($arr){
        $this->add(['uid'=>$arr['uid'], 'group_id' => $arr['group_id']]);
    }
}
