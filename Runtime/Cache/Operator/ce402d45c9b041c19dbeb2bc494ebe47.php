<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>

    <style>
        th,td{text-align: center;}
        .row-2{
            padding: 0px 10px;
        }

        .row-input-width{
            width: 347px!important;
        }
        .form-group{
            padding-right: 50px!important;
            width: 42%!important;
            margin-bottom: 15px!important;
        }

        .orderNo-item{
            padding-left: 28px;
        }
        .bill-time-item{
            padding-left: 14px;
        }
        .margin-left-29{
            margin-left: 29px;
        }
        .range-item{
            padding: 7px 10px;
            background-color: #eee;
        }

        .input-left{
            margin-left: -4px;
        }
        .select-btn{
            margin-top: -15px;
        }
        .total-item{
            text-align: right;
            padding: 0 3px 10px 3px;
            color: red;
        }
        ul{ padding: 0; }
    </style>
    <div class="page-header"><h1><i class="fa fa-home"></i> 首页 &gt; 财务管理 &gt; 员工销售业绩</h1></div>

    <div class="col-xs-12">

        <div class="tab-content">
            <div class="row" >
                <form action="" id="form" class="form-inline" style="margin-left:10px;" >
                    <input type="hidden" name="p" value="1" />
                    <div class="row-2" >
                        <div class="form-group">
                            <label for="start_time" class="bill-time-item">下单日期:</label>
                            <input type="text" name="start_time" id="start" value="<?php if($_GET['start_time']): echo ($_GET['start_time']); endif; ?>">
                            <span class="range-item input-left">至</span>
                            <input class="input-left" type="text" name="end_time" id="end" value="<?php if($_GET['end_time']): echo ($_GET['end_time']); endif; ?>">
                        </div>
                     </div>
                    <div class="row-2" >
                        <div class="form-group">
                            <label for="source_type" class="bill-time-item margin-left-29">门店:</label>
                            <select id="shop_id" name="shop_id" class="row-input-width">
                                <option value="">请选择</option>
                                <?php if(is_array($shop)): $i = 0; $__LIST__ = $shop;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value='<?php echo ($vo['reseller_id']); ?>' <?php if($_GET['shop_id'] == $vo['reseller_id'] ): ?>selected<?php endif; ?>><?php echo ($vo['reseller_name']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sales">销售人员:</label>
                            <input type="text" name="sales" value="<?php if($_GET['sales']): echo urldecode($_GET['sales']); endif; ?>" placeholder="请输入销售人员名称" />
                        </div>
                    </div>
                    <div class="col-xs-12" >
                        <button type="submit" class="btn btn-primary ">查询</button>
                        <button type="button" class="btn btn-primary" onclick="outExcel()">导出</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="tabbable">
  <!--           <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
                <li class="active"><a href="<?php echo U('Rule/admin_user_list');?>">员工列表</a></li>
                <li><a href="<?php echo U('Rule/add_admin');?>">添加员工</a></li>
            </ul> -->
            <div class="tab-content">
                <div class="total-item">总销售额：¥<?php echo ($all_total_money); ?></div>
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <tr>
                        <th>订单号</th>
                        <th>线路名称</th>
                        <th>下单日期</th>
                        <th>订单总额</th>
                        <th>销售人员</th>
                    </tr>
                    <?php if(is_array($list)): foreach($list as $key=>$v): ?><tr>
                            <td><?php echo ($v['order_num']); ?></td>
                            <td><?php echo ($v['line_name']); ?></td>
                            <td><?php echo (date('Y-m-d',$v['create_time'])); ?></td>
                            <td><?php echo ($v['total_money']); ?></td>
                            <td><?php echo ($v['name']); ?></td>
                        </tr><?php endforeach; endif; ?>
                    <tr>
                        <td colspan="7">
                            <!--分页样式-->
                            <ul class="pagination">
                                <?php echo ($show); ?>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<script src="/Tpl/Operator/js/jedate/jedate.js"></script>
<script>
//导出excel
    function outExcel(){
        $form = $('#form');
        $form.attr('action', '<?php echo U('outPerformanceExcel');?>');
        $form.submit();
        $form.attr('action', '');
    }
    /**
     * 绑定日期选择器
     * @param  {[obj]}    obj        [元素]
     * @param  {[string]} dateFormat [时间格式]
     */
    function dateFormat(obj, dateFormat){
        jeDate({
            dateCell: '#'+$(obj).attr('id'),
            format: dateFormat,
            isinitVal:false,
            isTime:true, //isClear:false,
            okfun:function(val){
            }
        });
    }
   dateFormat($('#start'), 'YYYY-MM-DD');
   dateFormat($('#end'), 'YYYY-MM-DD');
</script>