<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>

    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <style>
        th{text-align: center;}
        .table-condensed thead>tr>th, .table-condensed tbody>tr>th, .table-condensed tfoot>tr>th, .table-condensed thead>tr>td, .table-condensed tbody>tr>td, .table-condensed tfoot>tr>td{text-align:center;}
    </style>


    <div class="page-header"><h1>首页 > 财务管理 &gt;供应商线路结算</h1></div>
    <ul id="nav" class="nav nav-tabs">
      <li role="presentation"  <?php if($post['status'] == '-1'): ?>class="active"<?php endif; ?> ><a href="javascript:checkStatus(-1);">待结算</a></li>
      <li role="presentation"  <?php if($post['status'] == 1): ?>class="active"<?php endif; ?> ><a href="javascript:checkStatus(1);">结算中</a></li>
      <li role="presentation" <?php if($post['status'] == 2): ?>class="active"<?php endif; ?>  ><a href="javascript:checkStatus(2);">已结算</a></li>
    </ul>
    <div class="tab-content">
        <div class="row" >
            <form action="<?php echo U('financeList');?>" class="form-inline " method="get" style="margin-left:10px;" >
                <input type="hidden" name="status" value="<?php echo ($post["status"]); ?>" style="display: none;">
                <div class="form-group">
                    <label for="group_num">团号</label>
                    <input type="text" class="" name="group_num" id="group_num" placeholder="团号" value="<?php echo ($post["group_num"]); ?>">
                </div>
                <div class="form-group">
                    <label for="lineName">线路名称</label>
                    <input type="text" class="" name="lineName" id="lineName" placeholder="线路名称" value="<?php echo ($post["lineName"]); ?>">
                </div>
                <div class="form-group">
                    <label for="pushDepartment">成团日期</label>
                    <input type="text" class="" name="group_time" id="group_time" placeholder="成团日期" value="<?php echo ($post["group_time"]); ?>">
                </div>
                <button type="submit" class="btn btn-primary btn-sm">查询</button>
            </form>
        </div>


        <table class="table table-striped table-bordered table-hover table-condensed mt-15">
            <thead>
                <tr>
                    <th>团号</th>
                    <th>线路名称</th>
                    <th>总人数</th>
                    <th>团状态</th>
                    <th>成团日期</th>
                    <th>总结算金额（人民币）</th>
                    <th>操作人</th>
                    <th>结算日期</th>
                    <th>结算状态</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($vo["group_num"]); ?></td>
                        <td><?php echo ($vo["line_name"]); ?></td>
                        <td><?php echo ($vo["received_num"]); ?></td>
                        <td>
                            <?php switch($vo["group_status"]): case "0": ?>待处理<?php break;?>
                                <?php case "-1": ?>不成团<?php break;?>
                                <?php case "1": ?>已成团<?php break;?>
                                <?php case "2": ?>已出团<?php break;?>
                                <?php case "3": ?>已回团<?php break; endswitch;?>
                        </td>
                        <td>
                            <?php if($vo['group_time']): echo (date("Y-m-d",$vo["group_time"])); endif; ?>
                        </td>
                        <td><?php echo ($vo["closing_total_money"]); ?></td>
                        <td><?php echo ($vo["admin2_name"]); ?></td>
                        <td>
                            <?php if($vo['closing_time']): echo (date("Y-m-d",$vo["closing_time"])); endif; ?>
                        </td>
                        <td>
                            <?php switch($vo["closing_status"]): case "-1": ?>待结算<?php break;?>
                                <?php case "1": ?>结算中<?php break;?>
                                <?php case "2": ?>已结算<?php break;?>
                                <?php case "3": ?>确认完成结算<?php break; endswitch;?>
                        </td>
                        <td>
                            <a href="<?php echo U('groupCostDetail',array('id'=>$vo['group_id']));?>" class="btn btn-warning btn-xs">结算明细</a>
                            <a class="btn btn-primary btn-xs" href="<?php echo U('groupOrderDetail',array('id'=>$vo['group_id']));?>">订单详情</a>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                    <tr>
                        <td colspan="10">
                            <ul class="pagination"><?php echo ($show); ?></ul>
                        </td>
                    </tr>

            </tbody>
        </table>

    </div>


    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>
    <script>
        layui.use('laydate', function() {
            var laydate = layui.laydate;
            //常规用法
            laydate.render({
                elem: '#group_time'
            });
        });
        function checkStatus(status) {
            $('form')[0].reset();
            $('input[name="status"]').val(status);
            $('form').submit();
        }
    </script>