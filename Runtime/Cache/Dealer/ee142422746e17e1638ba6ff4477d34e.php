<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->

    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        .required{color:#FF9966}
        .thumbImg{height:200px;}
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Dealer/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>

<script>
    $(function () {
        var bodyH=$(document).height();
        try{
            parent.resetFrameHeight(bodyH);
        } catch(error){

        }
    })
</script>
</body>
</html>

    <div class="page-header"><h1><i class="fa fa-home"></i> 首页 &gt;菜单管理</h1></div>
    <div class="col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
                <li class="active"><a href="#home" data-toggle="tab">菜单列表</a></li>
                <li><a href="javascript:;" onclick="add()">添加菜单</a></li>
            </ul>
            <div class="tab-content">
                <form class="" action="<?php echo U('Nav/order');?>" method="post">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <tr>
                            <th width="5%">排序</th>
                            <th>菜单名</th>
                            <th>连接</th>
                            <th>操作</th>
                        </tr>
                        <?php if(is_array($data)): foreach($data as $key=>$v): ?><tr>
                                <td><input class="input-medium" style="width:40px;height:25px;" type="text"
                                           name="<?php echo ($v['id']); ?>" value="<?php echo ($v['order_number']); ?>"></td>
                                <td><?php echo ($v['_name']); ?></td>
                                <td><?php echo ($v['mca']); ?></td>
                                <td><a href="javascript:;" navId="<?php echo ($v['id']); ?>" navName="<?php echo ($v['name']); ?>"
                                       onclick="add_child(this)">添加子菜单</a> | <a href="javascript:;" navId="<?php echo ($v['id']); ?>"
                                                                                navName="<?php echo ($v['name']); ?>"
                                                                                navMca="<?php echo ($v['mca']); ?>"
                                                                                navIco="<?php echo ($v['ico']); ?>"
                                                                                onclick="edit(this)">修改</a> | <a
                                        href="javascript:if(confirm('确定删除？'))location='<?php echo U('Nav/delete',array('id'=>$v['id']));?>'">删除</a>
                                </td>
                            </tr><?php endforeach; endif; ?>
                        <tr>
                            <th><input class="btn btn-success" type="submit" value="排序"></th>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="bjy-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                    <h4 class="modal-title" id="myModalLabel"> 添加菜单</h4></div>
                <div class="modal-body">
                    <form id="bjy-form" class="form-inline" action="<?php echo U('Nav/add');?>" method="post"><input
                            type="hidden" name="pid" value="0">
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <tr>
                                <th width="12%">菜单名：</th>
                                <td><input class="input-medium" type="text" name="name"></td>
                            </tr>
                            <tr>
                                <th>连接：</th>
                                <td><input class="input-medium" type="text" name="mca"> 输入模块/控制器/方法即可 例如 Admin/Nav/index
                                </td>
                            </tr>
                            <tr>
                                <th>图标：</th>
                                <td><input class="input-medium" type="text" name="ico"> font-awesome图标 输入fa fa- 后边的即可
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td><input class="btn btn-success" type="submit" value="添加"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="bjy-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                    <h4 class="modal-title" id="myModalLabel"> 修改菜单</h4></div>
                <div class="modal-body">
                    <form id="bjy-form" class="form-inline" action="<?php echo U('Nav/edit');?>" method="post"><input
                            type="hidden" name="id">
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <tr>
                                <th width="12%">菜单名：</th>
                                <td><input class="input-medium" type="text" name="name"></td>
                            </tr>
                            <tr>
                                <th>连接：</th>
                                <td><input class="input-medium" type="text" name="mca"> 输入模块/控制器/方法即可 例如 Admin/Nav/index
                                </td>
                            </tr>
                            <tr>
                                <th>图标：</th>
                                <td><input class="input-medium" type="text" name="ico"> font-awesome图标 输入fa fa- 后边的即可
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td><input class="btn btn-success" type="submit" value="修改"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script>
        // 添加菜单
        function add() {
            $("input[name='name'],input[name='mca']").val('');
            $("input[name='pid']").val(0);
            $('#bjy-add').modal('show');
        }

        // 添加子菜单
        function add_child(obj) {
            var navId = $(obj).attr('navId');
            $("input[name='pid']").val(navId);
            $("input[name='name']").val('');
            $("input[name='mca']").val('');
            $("input[name='ico']").val('');
            $('#bjy-add').modal('show');
        }

        // 修改菜单
        function edit(obj) {
            var navId = $(obj).attr('navId');
            var navName = $(obj).attr('navName');
            var navMca = $(obj).attr('navMca');
            var navIco = $(obj).attr('navIco');
            $("input[name='id']").val(navId);
            $("input[name='name']").val(navName);
            $("input[name='mca']").val(navMca);
            $("input[name='ico']").val(navIco);
            $('#bjy-edit').modal('show');
        }
    </script>