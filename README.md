 **旅游智能CRM系统** 
 **介绍** <br>

有意者，可以扫底部二维码咨询

运营商平台登录地址：u:18600008881     p:123456
http://localhost/Operator/Login/login.html

供应商平台登录地址：u:18600008882     p:123456
http://localhost/Dealer/Login/login.html

分销商平台登录地址：u:18600008883     p:123456
http://localhost/reseller/Login/login.html



【旅游智能CRM系统】，为旅游公司,精心打造的管理游客的一款系统。
<br>

 **软件架构** 
<br>
Thinkphp 3.2.2 bootstrap
Apache 2.4.9 Nginx 1.1
php 5.4以上
Mysql 5.0
<br>
 **使用说明** 
 **1 【旅游智能CRM系统】** 
<br>
 **1.1总平台截图** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164211_8f846b6f_448924.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164219_d52e4234_448924.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164227_b37736a9_448924.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164235_73a29133_448924.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164243_1179f951_448924.png "5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164252_6a3521c2_448924.png "6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164302_00349674_448924.png "7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164310_cac0e7dc_448924.png "8.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164318_3261db3e_448924.png "9.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164327_e99aba10_448924.png "10.png")
 **1.2供应商平台截图** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/171051_e0ec9961_448924.png "21.png")
 **1.3分销商平台截图** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/171217_7349d8da_448924.png "31.png")
 **有意者可以扫码，联系作者** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0306/164413_96cacf05_448924.jpeg "11.jpeg")
(输入图片说明 扫码添加时， 附言： 旅游智能CRM系统）